<?php
class ModelExtensionModuleAutoship extends Model {


public function getProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "') AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return array(
				'product_id'       => $query->row['product_id'],
				'name'             => $query->row['name'],
				'description'      => $query->row['description'],
				'meta_title'       => $query->row['meta_title'],
				'meta_description' => $query->row['meta_description'],
				'meta_keyword'     => $query->row['meta_keyword'],
				'tag'              => $query->row['tag'],
				'model'            => $query->row['model'],
				'sku'              => $query->row['sku'],
				'upc'              => $query->row['upc'],
				'ean'              => $query->row['ean'],
				'jan'              => $query->row['jan'],
				'isbn'             => $query->row['isbn'],
				'mpn'              => $query->row['mpn'],
				'location'         => $query->row['location'],
				'quantity'         => $query->row['quantity'],
				'stock_status'     => $query->row['stock_status'],
				'image'            => $query->row['image'],
				'manufacturer_id'  => $query->row['manufacturer_id'],
				'manufacturer'     => $query->row['manufacturer'],
				'price'            => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
				'special'          => $query->row['special'],
				'reward'           => $query->row['reward'],
				'points'           => $query->row['points'],
				'tax_class_id'     => $query->row['tax_class_id'],
				'date_available'   => $query->row['date_available'],
				'weight'           => $query->row['weight'],
				'weight_class_id'  => $query->row['weight_class_id'],
				'length'           => $query->row['length'],
				'width'            => $query->row['width'],
				'height'           => $query->row['height'],
				'length_class_id'  => $query->row['length_class_id'],
				'subtract'         => $query->row['subtract'],
				'rating'           => round($query->row['rating']),
				'reviews'          => $query->row['reviews'] ? $query->row['reviews'] : 0,
				'minimum'          => $query->row['minimum'],
				'sort_order'       => $query->row['sort_order'],
				'status'           => $query->row['status'],
				'date_added'       => $query->row['date_added'],
				'date_modified'    => $query->row['date_modified'],
				'viewed'           => $query->row['viewed']
			);
		} else {
			return false;
		}
	}
	
	
	public function getProducts($data = array()) {
		$sql = "SELECT p.product_id, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND p.autoship = 1 AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special";

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
			} else {
				$sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
			}

			if (!empty($data['filter_filter'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
			} else {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
			}
		} else {
			$sql .= " FROM " . DB_PREFIX . "product p";
		}

		$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.autoship = 1  AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";
			} else {
				$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
			}

			if (!empty($data['filter_filter'])) {
				$implode = array();

				$filters = explode(',', $data['filter_filter']);

				foreach ($filters as $filter_id) {
					$implode[] = (int)$filter_id;
				}

				$sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
			}
		}

		if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
			$sql .= " AND (";

			if (!empty($data['filter_name'])) {
				$implode = array();

				$words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

				foreach ($words as $word) {
					$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
				}

				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}

				if (!empty($data['filter_description'])) {
					$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
				}
			}

			if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
				$sql .= " OR ";
			}

			if (!empty($data['filter_tag'])) {
				$implode = array();

				$words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_tag'])));

				foreach ($words as $word) {
					$implode[] = "pd.tag LIKE '%" . $this->db->escape($word) . "%'";
				}

				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}
			}

			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}

			$sql .= ")";
		}

		if (!empty($data['filter_manufacturer_id'])) {
			$sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
		}

		$sql .= " GROUP BY p.product_id";

		$sort_data = array(
			'pd.name',
			'p.model',
			'p.quantity',
			'p.price',
			'rating',
			'p.sort_order',
			'p.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
				$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
			} elseif ($data['sort'] == 'p.price') {
				$sql .= " ORDER BY (CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
			} else {
				$sql .= " ORDER BY " . $data['sort'];
			}
		} else {
			$sql .= " ORDER BY p.sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC, LCASE(pd.name) DESC";
		} else {
			$sql .= " ASC, LCASE(pd.name) ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$product_data = array();

		//print $sql;

		$query = $this->db->query($sql);

		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
		}

		return $product_data;
	}
	
	public function updateAutoship($data){
		$this->load->model("account/naxumcustomer");
		$customer_id = $this->model_account_naxumcustomer->getCurrentCustomerID();

		$query = $this->db->adaptor->query("select * from oc_autoship where customer_id = :customer_id",array(":customer_id"=>$customer_id));


		if ($query->num_rows > 0) {

			$rawDate = $data['deliverydate'];
			$dayToday =  date('d', strtotime($data['deliverydate']));	
			$nextDeliveryDate= $this->jjg_calculate_next_month( 

			$retval = $this->db->adaptor->query("UPDATE  oc_autoship SET deliverydate=:deliverydate,lastmodified=NOW(),total=:total,shipping_total = :stotal WHERE  customer_id=:customer_id",array(":deliverydate"=>$data['deliverydate'],":customer_id"=>$customer_id,":total"=>$data['total'],":stotal"=>$this->config->get("xen_regular_cost")));	
		
			$json = array("result_id"=>$query->row['id'],"result_text"=>"Successfully updated!!!!!","rows_affected"=>$retval->num_rows,"customerid"=>$customer_id,"query"=>"");	

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			
		}
		else
		{
			$retval = $this->db->adaptor->query("insert into oc_autoship(deliverydate,lastmodified,customer_id,total) values (:deliverydate,NOW(),:customer_id,:total)",array(":deliverydate"=>$data['deliverydate'],":customer_id"=>$customer_id,":total"=>$data['total']));
			if ($retval->num_rows > 0) {
					$query = $this->db->adaptor->query("select * from oc_autoship where customer_id = :customer_id",array(":customer_id"=>$customer_id));
					$json = array("result_id"=>$query->row['id']);		

					$this->response->addHeader('Content-Type: application/json');
					$this->response->setOutput(json_encode($json));					
			}
			
		}
	
	}
	
	public setNewDeliveryDate($data){
			$rawDate = $data['deliverydate'];
			$dayToday =  date('d', strtotime($data['deliverydate']));	
			$nextDeliveryDate= $this->jjg_calculate_next_month($rawDate); 

			$retval = $this->db->adaptor->query("UPDATE  oc_autoship SET deliverydate=:deliverydate,lastmodified=NOW() WHERE  customer_id=:customer_id",array(":deliverydate"=>$nextDeliveryDate,":customer_id"=>$customer_id));		
			return $retval;
	}
	
	public function updateAutoshipDetails($data){
		
		$this->db->adaptor->query("DELETE FROM oc_autoshipdetails where autoship_id = :id",array(":id"=>$data['autoship_id']));
	
		for ($i=0;$i<count($data['product_idss']);$i++)
		{
			$result = $this->db->adaptor->query("INSERT INTO oc_autoshipdetails (autoship_id,product_id,quantity,price,total) VALUES (:autoship_id,:product_id,:quantity,:price,:total)",array(":autoship_id"=>$data['autoship_id'],":product_id"=>$data['product_idss'][$i],":quantity"=>$data['quantities'][$i],":price"=>$data['prices'][$i],":total"=>$data['total_amounts'][$i]));			
			
		}
		
 
	}
	
	public function getAutoship(){

		$this->load->model("account/naxumcustomer");
		$customer_id = $this->model_account_naxumcustomer->getCurrentCustomerID();		
		$result = $this->db->adaptor->query("SELECT * FROM oc_autoship where customer_id = :id",array(":id"=>$customer_id));
		
		return $result->row;
	
	}

	public function getAutoshipDetails(){	

		$this->load->model("account/naxumcustomer");
		$customer_id = $this->model_account_naxumcustomer->getCurrentCustomerID();		
		$result = $this->db->adaptor->query("SELECT * FROM oc_autoship where customer_id = :id",array(":id"=>$customer_id));
		$autoship_id = $result->row['id'];
	
		$result2 = $this->db->adaptor->query("SELECT od.*,op.model as name FROM oc_autoshipdetails od LEFT JOIN oc_product op  on op.product_id = od.product_id WHERE autoship_id = :id",array(":id"=>$autoship_id));
		
		$rows = [];
		foreach ($result2->rows as $the_row) {
			$the_row['fprice'] =  $this->currency->format($the_row['price'], $this->session->data['currency']);
			$the_row['ftotal'] =  $this->currency->format($the_row['total'], $this->session->data['currency']);
			$rows[] = $the_row;
			
		}
		
		return $rows;
	
	}
	
	
	public function processPayment($auth_info){
		
		$this->load->library("naxumcart");
		if ($auth_info['billmethod'] == 'CC') {
				$res = $this->naxumcart->payingAutoshipCC($auth_info);
			}
			else {
				print "Ledger account processing \n";
				$res = $this->processPaymentLedger($auth_info);
			}
		return $res;
		
	}
	
	private function zerofill($mStretch, $iLength = 2){
		
			$sPrintfString = '%0' . (int)$iLength . 's';
			return sprintf($sPrintfString, $mStretch);
		}
			
	
	public function processPaymentLedger($data){
					$this->load->model('account/naxumcustomer');
					$naxumuser = $this->model_account_naxumcustomer->getCurrentLedgerBalance($data['customer_id']);

					$new_naxum_info = array();
					if ($data['total'] <= $naxumuser['current_balance'] ) {
						$new_naxum_info['updated_balance'] = $naxumuser['current_balance'] - $data['total'];
						$new_naxum_info['balance'] = $naxumuser['current_balance'] - $data['total'];
						
						$this->model_account_naxumcustomer->updateLedgerCurrentBalance($data['customer_id'], -1 * $data['total']);
						

						$this->model_account_naxumcustomer->updateLedgerBalance(array(":user_id"=>$data['customer_id'],":notes"=>"Autoship Execution from Cart",":amount"=> -1*$data['total'],":balance"=>$naxumuser['current_balance'],":updated_balance"=>$new_naxum_info['updated_balance'],":transaction_id"=> 0,":ledger_request_id"=>"",":received_from_user_id"=>"",":transferred_to_user_id"=>"",":commission_period_ids"=>"",":status"=>"Completed"));	
						return array("approved"=>1,"transaction_id"=>0);
					} else {
						return array("approved"=>0);	
					}
		}
	
	
	
	public function insertProcessAuth2Trans($data) {
		
		$this->load->library("naxumcart");
		
		$sql = "INSERT INTO `transactions` (`userid`, `sponsorid`, `billfname`, `billlname`, `billaddress`, `billcity`, `billstate`, `billzip`, `billcountry`, `businessacct`, `billbusiness`, `billmethod`, `ccnumber`, `ccexp`, `ccid`, `bankname`, `acctype`, `aba`, `bankacctnum`, `dlnum`, `dlstate`, `ssn`, `invoice`, `authcode`, `refnum`, `amount`, `itemid`, `description`, `transactiondate`, `ip`, `merchaccount`, `credited`, `status`, `email`, `error`, `reconid`, `requesttoken`, `trackid`, `billdate`, `type`) values ( :userid, :sponsorid, :billfname, :billlname, :billaddress, :billcity, :billstate, :billzip, :billcountry, :businessacct, :billbusiness, :billmethod, :ccnumber, :ccexp, :ccid, :bankname, :acctype, :aba, :bankacctnum, :dlnum, :dlstate, :ssn, :invoice, :authcode, :refnum, :amount, :itemid, :description, NOW(), :ip, :merchaccount, NULL, :status, :email, :error, :reconid, :requesttoken, :trackid, :billdate, :type)";

		$billinfo = $this->naxumcart->getBillingInformation($data['customer_id']);
		
		#refnum is the reference number of the authorizenet
		#invoice is the autoship id number

		$res = $this->db->adaptor->query($sql,array( ':userid'=>$data['customer_id'], ':sponsorid'=>$billinfo['sponsorid'], ':billfname'=>$billinfo['billfname'], ':billlname'=>$billinfo['billlname'], ':billaddress'=>$billinfo['billaddress'], ':billcity'=>$billinfo['billcity'], ':billstate'=>$billinfo['billstate'], ':billzip'=>$billinfo['billstate'], ':billcountry'=>$billinfo['billcountry'], ':businessacct'=>$billinfo['billbusiness'], ':billbusiness'=>$billinfo['billbusiness'], ':billmethod'=>$billinfo['billmethod'], ':ccnumber'=>$billinfo['ccnumber'], ':ccexp'=>$billinfo['ccexp'], ':ccid'=>$billinfo['ccid'], ':bankname'=>$billinfo['bankname'], ':acctype'=>$billinfo['acctype'], ':aba'=>$billinfo['aba'], ':bankacctnum'=>$billinfo['bankacctnum'], ':dlnum'=>$billinfo['dlnum'], ':dlstate'=>$billinfo['dlstate'], ':ssn'=>$billinfo['ssn'], ':invoice'=>"OCA-".$this->zerofill($data['id'],4), ':authcode'=>'', ':refnum'=>$data['transaction_id'], ':amount'=>$data['total_amount'], ':itemid'=>'', ':description'=>'Opencart Autoship Charging ', ':ip'=>'', ':merchaccount'=>'boa',  ':status'=>'Approved', ':email'=>$billinfo['email'], ':error'=>0, ':reconid'=>0, ':requesttoken'=>0, ':trackid'=>0, ':billdate'=>(new DateTime($data['deliverydate']))->format('d'), ':type'=>'product'));		
		
	
		return $this->db->adaptor->getLastId();
		
	
	}

	public function insert2ShoppingPurchases($data,$trans_id) {	
		
		if($data['billmethod'] == "LEDGER"){
			        $sql = "UPDATE ledger_transaction_history SET transaction_id = :id WHERE user_id = :userId"; 
			        $this->db->adaptor->query($sql,array(":id"=>$trans_id,":userId"=>$data['customer_id']));
		
		}
		
		
		$sql = "INSERT INTO `shoppingcart_purchases` ( `userid`, `productid`, `quantity`, `purchasedate`, `billdate`, `enrollment`, `backoffice`, `recurperiod`, `active`, `lasttransdate`, `tranid`) VALUES ( :userid, :productid, :quantity, NOW(), NOW(), :enrollment, :backoffice, :recurperiod, :active, NOW(), :tranid)";
		
		$result = $this->db->adaptor->query("select ocd.*,oldp.old_id from oc_autoshipdetails ocd left join oc_oldproduct oldp on oldp.new_id = ocd.product_id where ocd.autoship_id = :id",array(":id"=>$data['id']));
		
		foreach ($result->rows as $therow) {
			
				print "Printing the row ";
				print_r($therow);
		
				$res = $this->db->adaptor->query($sql,array(':userid'=>$data['customer_id'], ':productid'=>$therow['old_id'], ':quantity'=>$therow['quantity'],  ':enrollment'=>0, ':backoffice'=>0, ':recurperiod'=>0, ':active'=> 1,':tranid'=>$trans_id));
				print_r($res);
	
		}
	
	}
	
	
	public function processTransactions($data){	
		$result = $this->db->adaptor->query("select ocd.*,oldp.old_id from oc_autoshipdetails ocd left join oc_oldproduct oldp on oldp.new_id = ocd.product_id where ocd.autoship_id = :id",array(":id"=>$data['id']));

		$sql = "UPDATE shoppingcart_purchases set quantity = :quantity,purchasedate= NOW(),billdate=NOW(),active=1,lasttransdate=NOW(),tranid = :tranid WHERE productid = :productid and userid = :userid";
		
		//$sql = "INSERT INTO `shoppingcart_purchases` ( `userid`, `productid`, `quantity`, `purchasedate`, `billdate`, `enrollment`, `backoffice`, `recurperiod`, `active`, `lasttransdate`, `tranid`) VALUES ( :userid, :productid, :quantity, NOW(), NOW(), :enrollment, :backoffice, :recurperiod, :active, NOW(), :tranid)";		
		foreach ($result->rows as $therow) {
				$data['total_amount'] = $therow['total'];
				$trans_id = $this->insertProcessAuth2Trans($data);
				$res = $this->db->adaptor->query($sql,array(':userid'=>$data['customer_id'], ':productid'=>$therow['old_id'], ':quantity'=>$therow['quantity'], ':tranid'=>$trans_id));
				$data['trans_id'] = $trans_id;
				$this->updateTransactionHistory($data);
				print_r($res);	
		}		

		$data['total_amount'] =  $this->config->get("xen_regular_cost");
		$trans_id = $this->insertProcessAuth2Trans($data);
		$data['trans_id'] = $trans_id;
		$this->updateTransactionHistory($data);

	}
	
	
	public function updateTransactionHistory($data) {
		$sql = "INSERT INTO oc_autoship_history (transaction_id,autoship_id,dateupdated) values (:transaction_id,:autoship_id,NOW())";
		$this->db->adaptor->query($sql,array(":transaction_id"=>$data['trans_id'],":autoship_id"=>$data['id']));
	}
	
	
	public function getTodaysCron(){
		$result = $this->db->adaptor->query("select oaut.*,b.billmethod from oc_autoship oaut left join billing b on b.userid = oaut.customer_id WHERE day(deliverydate)= day(NOW())");
		return $result->rows;
	
	}
	
	public function processTodays_ScheduledAutoship(){
		$result = $this->db->adaptor->query("select oaut.*,b.billmethod from oc_autoship oaut left join billing b on b.userid = oaut.customer_id WHERE day(deliverydate)= day(NOW())");
		
		foreach ($result->rows as $the_row) {			
				
				$res = $this->processPayment($the_row);
				
				
				if ($res['approved']) {
					$the_row['transaction_id'] = $res['transaction_id'];
					$this->processTransactions($the_row);
					
				}
				else
				{
					print "Failed transaction here\n";
				}
		
		}
		
	}
	
	
	#from https://www.jeffgeerling.com/blogs/jeff-geerling/php-calculating-monthly
	function jjg_calculate_next_month($start_date = FALSE) {
		  if ($start_date) {
			$now = strtotime($start_date); // Use supplied start date.
		  } else {
			$now = time(); // Use current time.
		  }

		  // Get the current month (as integer).
		  $current_month = date('n', $now);

		  // If the we're in Dec (12), set current month to Jan (1), add 1 to year.
		  if ($current_month == 12) {
			$next_month = 1;
			$plus_one_month = mktime(0, 0, 0, 1, date('d', $now), date('Y', $now) + 1);
		  }
		  // Otherwise, add a month to the next month and calculate the date.
		  else {
			$next_month = $current_month + 1;
			$plus_one_month = mktime(0, 0, 0, date('m', $now) + 1, date('d', $now), date('Y', $now));
		  }

		  $i = 1;
		  // Go back a day at a time until we get the last day next month.
		  while (date('n', $plus_one_month) != $next_month) {
			$plus_one_month = mktime(0, 0, 0, date('m', $now) + 1, date('d', $now) - $i, date('Y', $now));
			$i++;
		  }

		return date("Y-m-d",$plus_one_month);
		  
	}


}
