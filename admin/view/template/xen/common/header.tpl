<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title> Cart <?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />



<script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="/js/jquery.tools.min.js" type="text/javascript"></script>
<script src="/js/hideElem.js" type="text/javascript"></script>
<script src="/js/notif.js" type="text/javascript"></script>
<link href="/css/bootstrap-datetimepicker.css" rel="stylesheet" media="screen">
<link href="/css/star-rating.min.css" rel="stylesheet" media="screen">
<script src="/js/respond.min.js"></script>
<script src="/js/bootstrap-maxlength.min.js"></script>
<script type="text/javascript" src="/js/jquery.autocomplete.js"></script>
<script src="/js/sweetalert.js"></script>
<!--cdns 
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
-->

<!-- <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> -->
<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<link href="/css/common/editor/froala_editor.min.css" rel="stylesheet" type="text/css" />
<link href="/css/common/editor/froala_style.min.css" rel="stylesheet" type="text/css" />
    
  
  
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!--end cdns -->


<!--fonts -->
<link href='//fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Oswald" />

<!--fonts -->
<!-- start customized views -->
<link href="/css/systems/xen/custom.css?session-30166725751860" rel="stylesheet">
<link href="/css/systems/xen/common.css?session-30166725751860" rel="stylesheet" media="screen">
 <!-- end customized views --> 

 
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


    <!-- MENUS -->
    
<link href="/css/systems/xen/menu.css?session-30166725751860" rel="stylesheet">
<link href="/css/systems/xen/menu-custom.css?session-30166725751860" rel="stylesheet">
<link href="/css/global.min.css?session-30166725751860" rel="stylesheet">

    <!-- MENUS -->
    
    
<script src="/js/moment.min.js"></script>


<script src="/js/livestamp.min.js"></script>
<script src="/js/bootstrap-notify.min.js"></script>


<script src="/js/star-rating.min.js"></script>
<link href="/css/sweetalert.css" rel="stylesheet">
<script type="text/javascript" src="/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>



<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>


<link rel="stylesheet" href="//cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css"/>
<script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>

<script src="/js/jquery.cookie.js"></script>
<script src="/js/jquery.md5.js"></script>



<link type="text/css" href="/cart/admin/view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />
<?php foreach ($styles as $style) { ?>
<link type="text/css" href="<?php echo $style['href']; ?>" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="/cart/admin/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>


<script type="text/javascript">
	jQuery(document).ready(function(){
    
		jQuery.ajax({'url':"/cart_header.cgi",success:function(data){
			var pattern = /href\=\"\//g;
			the_string = data.replace(pattern, "href=\"");
			pattern = /href\=\"/g;
			the_string = the_string.replace(pattern, "href=\"/");
            the_string = the_string.substring(the_string.indexOf('<nav class="navbar navbar-default top-nav">'));		
            //the_string = the_string.split('<nav class="navbar navbar-default top-nav">').pop();
			jQuery("#header").html(the_string);
	},
    complete:function(){
        
        
        }
    });    
    
    

});


</script>

<style>

#menu {    margin-bottom: 25px;    margin-top: 29px;}

</style>
</head>
<body>
<div id="container">
<header id="header" class="navbar navbar-static-top">
    Loading...
</header>
