<?php
class ControllerCommonTutorial extends Controller {
    public function test() {
		if (!defined("OPENCART_CLI_MODE") || OPENCART_CLI_MODE === FALSE) {
			echo "Intruder alert."; exit;
        }

        oc_cli_output("Hello, world!");
    }
}
?>
