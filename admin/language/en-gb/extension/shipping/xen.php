<?php
// Heading
$_['heading_title']    = 'Xen Shipping';

// Text
$_['text_shipping']    = 'Shipping';
$_['text_success']     = 'Success: You have modified Xenesta rate shipping!';
$_['text_edit']        = 'Edit xen Rate Shipping';

// Entry
$_['entry_regular_cost']       = 'Regular Cost';
$_['entry_book_cost']       = 'Book Cost';
$_['entry_book_cost2']       = 'Cost for Two Books';
$_['entry_tax_class']  = 'Tax Class';
$_['entry_geo_zone']   = 'Geo Zone';
$_['entry_status']     = 'Status';
$_['entry_sort_order'] = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Xen rate shipping!';
