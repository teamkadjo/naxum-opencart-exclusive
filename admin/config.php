<?php
// HTTP
define('HTTP_SERVER', 'https://office.xenestalife.com/cart/admin/');
define('HTTP_CATALOG', 'https://office.xenestalife.com/cart/');

// HTTPS
define('HTTPS_SERVER', 'https://office.xenestalife.com/cart/admin/');
define('HTTPS_CATALOG', 'https://office.xenestalife.com/cart/'); 

// DIR
define('DIR_APPLICATION', '/var/rep/code/office/cart/admin/');
define('DIR_SYSTEM', '/var/rep/code/office/cart/system/');
define('DIR_IMAGE', '/var/rep/code/office/cart/image/');
define('DIR_LANGUAGE', '/var/rep/code/office/cart/admin/language/');
define('DIR_TEMPLATE', '/var/rep/code/office/cart/admin/view/template/');
define('DIR_CONFIG', '/var/rep/code/office/cart/system/config/');
define('DIR_CACHE', '/var/rep/code/office/cart/system/storage/cache/');
define('DIR_DOWNLOAD', '/var/rep/code/office/cart/system/storage/download/');
define('DIR_LOGS', '/var/rep/code/office/cart/system/storage/logs/');
define('DIR_MODIFICATION', '/var/rep/code/office/cart/system/storage/modification/');
define('DIR_UPLOAD', '/var/rep/code/office/cart/system/storage/upload/');
define('DIR_CATALOG', '/var/rep/code/office/cart/catalog/');

// DB
define('DB_DRIVER', 'mpdo');
define('DB_HOSTNAME', 'dbserver');
define('DB_USERNAME', 'apache');
define('DB_PASSWORD', 'fr1ckl3');
define('DB_DATABASE', 'xen');
define('DB_PORT', '3306');  
define('DB_PREFIX', 'oc_');
