<?php
// Text
$_['text_title'] = 'Pay using the Existing Saved Credit Card ';
$_['text_credit_card'] = 'Pay using Saved Credit Card on System';
$_['entry_cc_number'] = 'CC Number (last 4 digit): ';
$_['entry_cc_cvv2']		   = 'Card Security Code (CVV2)';
$_['entry_cc_expire_date'] = 'Card Expiry Date';
$_['entry_cc_expire_date1']		   = 'Card Expiry Date (info is in system)';
$_['entry_cc_cvv2_place']  = 'CVV2';
