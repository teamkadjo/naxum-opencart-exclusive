<?php
class ModelAccountAddress extends Model {
	public function addAddress($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$this->customer->getId() . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', city = '" . $this->db->escape($data['city']) . "', zone_id = '" . (int)$data['zone_id'] . "', country_id = '" . (int)$data['country_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "'");

		$address_id = $this->db->getLastId();

		if (!empty($data['default'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		}

		return $address_id;
	}

	public function editAddress($address_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "address SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', city = '" . $this->db->escape($data['city']) . "', zone_id = '" . (int)$data['zone_id'] . "', country_id = '" . (int)$data['country_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "' WHERE address_id  = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		if (!empty($data['default'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		}
	}

	public function deleteAddress($address_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");
	}

	public function getAddress($address_id,$is_shipping_addr=0) {
		
		$addresses = $this->getAddresses($is_shipping_addr);
		return $addresses[$address_id];
		
	/*	$address_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		if ($address_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}

			$address_data = array(
				'address_id'     => $address_query->row['address_id'],
				'firstname'      => $address_query->row['firstname'],
				'lastname'       => $address_query->row['lastname'],
				'company'        => $address_query->row['company'],
				'address_1'      => $address_query->row['address_1'],
				'address_2'      => $address_query->row['address_2'],
				'postcode'       => $address_query->row['postcode'],
				'city'           => $address_query->row['city'],
				'zone_id'        => $address_query->row['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $address_query->row['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'custom_field'   => json_decode($address_query->row['custom_field'], true)
			);

			return $address_data;
		} else {
			return false;
		} */
	}

	public function getAddresses($is_shipping_addr=0) {
		$address_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		$count = 0;

		foreach ($query->rows as $result) {
			$country_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "country` WHERE country_id = '" . (int)$result['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "zone` WHERE zone_id = '" . (int)$result['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}


		if ($count == 0) {
		
			if (!$is_shipping_addr) {
					
			$the_address = $this->naxumcart->getBillingInformation((int)$this->customer->getId());
			$address_data[$result['address_id']] = array(
				'address_id'     => $result['address_id'],
				'firstname'      => $the_address['billfname'],
				'lastname'       => $the_address['billlname'],
				'company'        => "",
				'address_1'      => $the_address['billaddress'],
				'address_2'      => "",
				'postcode'       => $the_address['billzip'],
				'city'           => $the_address['billcity'],
				'zone_id'        => "",
				'zone'           => $the_address['billstate'],
				'zone_code'      => "",
				'country_id'     => "",
				'country'        => $the_address['billcountry'],
				'iso_code_2'     => "",
				'iso_code_3'     => "",
				'address_format' => "",
				'custom_field'   => "",

			);
			
		} else {


			

			$the_address = $this->naxumcart->getBillingInformation((int)$this->customer->getId());
			
			if (isset($the_address['shipaddress'])) {
				$address_data[$result['address_id']] = array(
					'address_id'     => $result['address_id'],
					'firstname'      => $the_address['shipfname'],
					'lastname'       => $the_address['shiplname'],
					'company'        => "",
					'address_1'      => $the_address['shipaddress'],
					'address_2'      => "",
					'postcode'       => $the_address['shipzip'],
					'city'           => $the_address['shipcity'],
					'zone_id'        => "",
					'zone'           => $the_address['shipstate'],
					'zone_code'      => "",
					'country_id'     => "",
					'country'        => $the_address['shipcountry'],
					'iso_code_2'     => "",
					'iso_code_3'     => "",
					'address_format' => "",
					'custom_field'   => "",

				);
			} else {

			// if shipping address is empty use billing address
			$address_data[$result['address_id']] = array(
				'address_id'     => $result['address_id'],
				'firstname'      => $the_address['billfname'],
				'lastname'       => $the_address['billlname'],
				'company'        => "",
				'address_1'      => $the_address['billaddress'],
				'address_2'      => "",
				'postcode'       => $the_address['billzip'],
				'city'           => $the_address['billcity'],
				'zone_id'        => "",
				'zone'           => $the_address['billzone'],
				'zone_code'      => "",
				'country_id'     => "",
				'country'        => $the_address['billcountry'],
				'iso_code_2'     => "",
				'iso_code_3'     => "",
				'address_format' => "",
				'custom_field'   => "",

			);
				
			}

			
		}
		
		
		
		
		}

		else {
			$address_data[$result['address_id']] = array(
				'address_id'     => $result['address_id'],
				'firstname'      => $result['firstname'],
				'lastname'       => $result['lastname'],
				'company'        => $result['company'],
				'address_1'      => $result['address_1'],
				'address_2'      => $result['address_2'],
				'postcode'       => $result['postcode'],
				'city'           => $result['city'],
				'zone_id'        => $result['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $result['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'custom_field'   => json_decode($result['custom_field'], true)

			);
		}
			
			$count ++;
			
		}

		return $address_data;
	}

	public function getTotalAddresses() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->row['total'];
	}
    
    

    ### BILLING ADDRESS FROM BILLING TABLE
    public function getBillingAddress($customerId){
        $address_query = $this->db->query("SELECT billfname,billlname,billaddress, billcity,billstate, billzip, billcountry FROM billing WHERE userid = '" . (int)$customerId . "' ");
        $billAddress = "";
		if ($address_query->num_rows) {
            $billAddress =$address_query->row['billfname']." ".$address_query->row['billlname']." ,".$address_query->row['billaddress']. " , ".$address_query->row['billcity']." , ".$address_query->row['billstate']." , ".$address_query->row['billzip']." ,".$address_query->row['billcountry'];
            
        }
        return $billAddress;
        
        
    }
    
     ### SHIPPING ADDRESS FROM SHIPPING TABLE
    public function getShippingAddress($customerId){
        $address_query = $this->db->query("SELECT  IFNULL(shipfname,billfname) as shipfname,IFNULL(shiplname,billlname) as shiplname,IFNULL(shipaddress,billaddress) as shipaddress, IFNULL(shipcity,billcity) as shipcity, IFNULL(shipstate,billstate) as shipstate,
  IFNULL(shipzip,billzip) as shipzip, IFNULL(shipcountry,billcountry) as shipcountry FROM billing WHERE userid = '" . (int)$customerId . "' ");
        $shipAddress = "";
		if ($address_query->num_rows) {
            $shipAddress =$address_query->row['shipfname']." ".$address_query->row['shiplname']." ".$address_query->row['shipaddress']. " , ".$address_query->row['shipcity']." , ".$address_query->row['shipstate']." , ".$address_query->row['shipzip']." ,".$address_query->row['shipcountry'];
            
        }
        return $shipAddress;
        
        
    }
    ### UPDATE BILLING ADDRESS TO BILLING TABLE
    public function updateBillingAddress($data,$customerId){
        $address_query = $this->db->query("SELECT id FROM billing WHERE userid = '" . (int)$customerId . "' ");
        
        ###COUNTRY
        $this->load->model('localisation/country');
		$country_query = $this->model_localisation_country->getCountry((int)$data['country_id']);
        $country= $country_query['name'];
        
        ###STATE
        $this->load->model('localisation/zone');
		$zone_query = $this->model_localisation_zone->getZone((int)$data['zone_id']);
        $zone= $zone_query['name'];
        
        ###UPDATE BILL INFO IS REQUIRED ON SIGNUP
		if ($address_query->num_rows) {
            	
            
                     // billfname,billlname, billaddress, billcity,billstate, billzip, billcountry FROM billing WHERE userid ='20';
            $this->db->query("UPDATE billing SET billfname = '" . $this->db->escape($data['firstname']) . "', billlname = '" . $this->db->escape($data['lastname']) . "', billaddress = '" . $this->db->escape($data['address_1']) . "', billzip = '" . $this->db->escape($data['postcode']) . "', billcity = '" . $this->db->escape($data['city']) . "', billstate = '" . $zone . "', billcountry = '" . $country . "'  WHERE  userid = '" . (int)$this->customer->getId() . "'");

        }/*else{
           
            $this->db->query("INSERT INTO billing SET userid = '" . (int)$this->customer->getId() . "', billfname = '" . $this->db->escape($data['firstname']) . "', billlname = '" . $this->db->escape($data['lastname']) . "', billaddress = '" . $this->db->escape($data['address_1']) . "', billzip = '" . $this->db->escape($data['postcode']) . "', billcity = '" . $this->db->escape($data['city']) . "', billstate = '" . $zone . "', billcountry = '" . $country . "' ");
        }*/
    }
    
    
     ### UPDATE SHIPPING ADDRESS TO BILLING TABLE
    public function updateShippingAddress($data,$customerId){
        $address_query = $this->db->query("SELECT id FROM billing WHERE userid = '" . (int)$customerId . "' ");
        
        ###COUNTRY
        $this->load->model('localisation/country');
		$country_query = $this->model_localisation_country->getCountry((int)$data['country_id']);
        $country= $country_query['name'];
        
        ###STATE
        $this->load->model('localisation/zone');
		$zone_query = $this->model_localisation_zone->getZone((int)$data['zone_id']);
        $zone= $zone_query['name'];
        
        ###UPDATE SHIP INFO IS REQUIRED ON SIGNUP
		if ($address_query->num_rows) {
            	
            
                     // shipfname, shiplname, shipaddress,shipcity,shipstate,shipzip,shipcountry FROM billing WHERE userid ='20';
            $this->db->query("UPDATE billing SET shipfname = '" . $this->db->escape($data['firstname']) . "', shiplname = '" . $this->db->escape($data['lastname']) . "', shipaddress = '" . $this->db->escape($data['address_1']) . "', shipzip = '" . $this->db->escape($data['postcode']) . "', shipcity = '" . $this->db->escape($data['city']) . "', shipstate = '" . $zone . "', shipcountry = '" . $country . "'  WHERE  userid = '" . (int)$this->customer->getId() . "'");

        }
    }
    
    
    
    
  
}
