<?php
class ModelAccountNaxumCustomer extends Model {
	public function getNaxumCustomer($user_id) {
		$query = $this->db->query("SELECT id, sponsorid,site,id as customer_group_id,fname as firstname,lname as lastname,email,evephone as telephone,fax,'' as custom_field FROM users WHERE id = '" . (int)$user_id . "'");
		return $query->row;
	}


	public function getNaxumUserInfo($user_id) {
			$query = $this->db->query("SELECT * FROM users WHERE id = '" . (int)$user_id . "'");
			return $query->row;
		}



    public function updateCreditCardInfo($data){
        
            $month = $data['cc_expire_date_month'];
            $year = $data['cc_expire_date_year'];
            $twodigityear = substr( $year, -2);
            $ccnumber = $data['cc_number'];
            @$encryptedcc = @exec ( "/usr/bin/perl /var/rep/code/office/crypt.pl ".$ccnumber." en");
            $userid = $this->getCurrentCustomerID();
            $ccexp = $month.$twodigityear;  
            $fname = $data['cc_owner_fname'];
            $lname =$data['cc_owner_lname'];
                
            #UPDATE IF CCNUMBER AND CCEXP AND FNAME AND LNAME ARE NOT NULL    
            if(!($ccnumber == "" or is_null($ccnumber)) and !($ccexp == "" or is_null($ccexp)) ){
                $sql = "UPDATE billing set ccnumber=:ccnumber,ccexp = :ccexp where userid = :userid";            
                $this->db->adaptor->query($sql,array(":userid"=>$userid,":ccnumber"=>$encryptedcc,":ccexp"=>$ccexp));  
            }
        
        }

	public function getCurrentCustomerID(){
		$userinfo = explode("|",$_COOKIE['LOGIN']);
		 return $userinfo[0];
	}



	public function getCurrentLedgerBalance($user_id){			
		$query = $this->db->adaptor->query("select * from ledger_current_balance where user_id = :balance",array(":balance"=>$user_id));
		return $query->row;	
	}


	public function insertTransaction($data){
		$result = $this->db->adaptor->query("INSERT INTO `transactions` (`userid`,`sponsorid`,`billfname`,`billlname`,`billaddress`,`billcity`,`billstate`,`billcountry`,`amount`,`itemid`,`description`,`transactiondate`,`is_rank_processed`,`new`,`inclannualfee`,`inclsetupfee`) VALUES (:userid,:sponsorid,:billfname,:billlname,:billaddress,:billcity,:billstate,:billcountry,:amount,:itemid,:description,NOW(),:is_rank_processed,:new,:inclannualfee,:inclsetupfee)",$data);
		$lastinsertid  = $this->db->adaptor->getLastId();
		return $lastinsertid;
	}
	
	public function updateLedgerBalance($data) {
		$sql = "INSERT INTO ledger_transaction_history (user_id, notes, amount, balance, updated_balance, transaction_id, ledger_request_id,received_from_user_id, transferred_to_user_id, commission_period_ids, last_updated, status) VALUES   (:user_id, :notes, :amount, :balance, :updated_balance, :transaction_id, :ledger_request_id, :received_from_user_id, :transferred_to_user_id, :commission_period_ids, now(), :status)";
		$result = $this->db->adaptor->query($sql,$data);
		return $result;
	}
	
    public function updateLedgerCurrentBalance($userId, $amount){
        $sql = "UPDATE ledger_current_balance SET current_balance = current_balance + :amount , last_updated = now()
                WHERE user_id = :userId";       
        $this->db->adaptor->query($sql,array(":userId"=>$userId,":amount"=>$amount));
    }	
	
	


}
