<?php
class ModelExtensionPaymentXenestaledger extends Model {
	public function getMethod($address, $total) {
		$this->load->language('extension/payment/xenestaledger');
		


		$customer_id = explode( '|', $_COOKIE['LOGIN'] )[0]; 		
		$query = $this->db->query("SELECT * FROM ledger_current_balance where user_id = '".$customer_id."'");
		$current_ledger_total = $query->row['current_balance'];
		$total_balance = number_format ($query->row['current_balance'],2 );
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('xenestaledger_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if ((($this->config->get('xenestaledger_total') > 0) && ($this->config->get('xenestaledger_total') >= $total))  || ($current_ledger_total  < $total)) {
			$status = false;
		} elseif (!$this->cart->hasShipping()) {
			$status = false;
		} elseif (!$this->config->get('xenestaledger_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$method_data = array(
				'code'       => 'xenestaledger',
				'title'      => $this->language->get('text_title')." with current balance of <strong>(USD ".$total_balance.")</strong>",
				'terms'      => '',
				'sort_order' => $this->config->get('xenestaledger_sort_order')
			);
		}

		return $method_data;
	}
}
