<?php
class ModelExtensionPaymentXenestaCard extends Model {
	public function getMethod($address, $total) {
		$this->load->language('extension/payment/xenestacard');

		$customer_id = explode( '|', $_COOKIE['LOGIN'] )[0]; 		
		$query = $this->db->query("select ccnumber from billing where  userid = '".$customer_id."'");
		
		$ccnumber  = $query->row['ccnumber'];
		ob_start();
		@$lastfewnumber = exec ( "/usr/bin/perl /var/rep/code/office/crypt.pl ".$ccnumber );
		ob_clean();
		$lastfewnumber = substr($lastfewnumber,-4);
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('xenestacard_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (($this->config->get('xenestacard_total') > 0) && ($this->config->get('xenestacard_total') >= $total)) {
			$status = false;
		} elseif (!$this->cart->hasShipping()) {
			$status = false;
		} elseif (!$this->config->get('xenestacard_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$method_data = array(
				'code'       => 'xenestacard',
				'title'      => $this->language->get('text_title')." with last 4 digit *".$lastfewnumber,
				'terms'      => '',
				'sort_order' => $this->config->get('xenestacard_sort_order')
			);
		}

		return $method_data;
	}
}
