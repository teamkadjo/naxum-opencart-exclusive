<?php
class ModelExtensionShippingXen extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/xen');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('xen_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if (!$this->config->get('xen_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['xen'] = array(
				'code'         => 'xen.xen',
				'title'        => $this->language->get('text_description'),
				'cost'         => $this->getComputation(null),
				'tax_class_id' => $this->config->get('xen_tax_class_id'),
				'text'         => $this->currency->format($this->getComputation(null),$this->session->data['currency']) //$this->currency->format($this->tax->calculate($this->config->get('xen_cost'), $this->config->get('xen_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'])
			);

			$method_data = array(
				'code'       => 'xen',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('xen_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}


	function getComputation($data){
		
		$user_id = $this->customer->customer_id;

		$query = $this->db->adaptor->query("select oc.product_id,opc.category_id,oc.quantity from oc_cart oc left join oc_product_to_category opc on oc.product_id = opc.product_id where oc.customer_id = :customer_id",array(":customer_id"=>$user_id));


		$regular_count = 0;
		$book_count = 0;
		$book_quantity = 0;
		//sample pack memory works
		if (($query->num_rows == 1) && ($query->rows[0]['product_id'] == 4)) {

			return 0;
		}
        //sample pack x-fuel retail
        if (($query->num_rows == 1) && ($query->rows[0]['product_id'] == 89)) {

			return 0;
		}
        //sample pack x-fuel preferred
        if (($query->num_rows == 1) && ($query->rows[0]['product_id'] == 90)) {

			return 0;
		}
		
		foreach ($query->rows as $row) {
			if ($row['category_id'] == 2) {
				$book_quantity += $row['quantity'] ;
				$book_count += $row['quantity'];
			}
			else 
			{
				$regular_count += 1;
			}
		}


		$regular_rate = floatval($this->config->get("xen_regular_cost")) + floatval($this->config->get("xen_book_cost"));
		$shipping_cost = floatval($this->config->get("xen_regular_cost"));		
		$book_shipping_cost = floatval($this->config->get("xen_book_cost"));
		$book_shipping_cost2 = floatval($this->config->get("xen_book_cost2"));
		if ($regular_count >= 1 && $book_count < 3) {
			return $shipping_cost ;
		}
		else {
			if ($book_count == 1) {
				return $book_shipping_cost;		
	
			}
			if ($book_count >= 3) {
				return  $shipping_cost ;			
			}
			elseif (($book_count == 2) && ($book_quantity <=2)) {
				return $book_shipping_cost2;
			}
			elseif (($book_count == 2) && ($book_quantity >=3)) {
				return $shipping_cost ;			
			}		

		}

	}



}
