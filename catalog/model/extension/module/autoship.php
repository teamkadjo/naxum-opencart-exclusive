<?php
class ModelExtensionModuleAutoship extends Model {


public function getProduct($product_id) {
		$query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND pr.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "') AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return array(
				'product_id'       => $query->row['product_id'],
				'name'             => $query->row['name'],
				'description'      => $query->row['description'],
				'meta_title'       => $query->row['meta_title'],
				'meta_description' => $query->row['meta_description'],
				'meta_keyword'     => $query->row['meta_keyword'],
				'tag'              => $query->row['tag'],
				'model'            => $query->row['model'],
				'sku'              => $query->row['sku'],
				'upc'              => $query->row['upc'],
				'ean'              => $query->row['ean'],
				'jan'              => $query->row['jan'],
				'isbn'             => $query->row['isbn'],
				'mpn'              => $query->row['mpn'],
				'location'         => $query->row['location'],
				'quantity'         => $query->row['quantity'],
				'stock_status'     => $query->row['stock_status'],
				'image'            => $query->row['image'],
				'manufacturer_id'  => $query->row['manufacturer_id'],
				'manufacturer'     => $query->row['manufacturer'],
				'price'            => ($query->row['discount'] ? $query->row['discount'] : $query->row['price']),
				'special'          => $query->row['special'],
				'reward'           => $query->row['reward'],
				'points'           => $query->row['points'],
				'tax_class_id'     => $query->row['tax_class_id'],
				'date_available'   => $query->row['date_available'],
				'weight'           => $query->row['weight'],
				'weight_class_id'  => $query->row['weight_class_id'],
				'length'           => $query->row['length'],
				'width'            => $query->row['width'],
				'height'           => $query->row['height'],
				'length_class_id'  => $query->row['length_class_id'],
				'subtract'         => $query->row['subtract'],
				'rating'           => round($query->row['rating']),
				'reviews'          => $query->row['reviews'] ? $query->row['reviews'] : 0,
				'minimum'          => $query->row['minimum'],
				'sort_order'       => $query->row['sort_order'],
				'status'           => $query->row['status'],
				'date_added'       => $query->row['date_added'],
				'date_modified'    => $query->row['date_modified'],
				'viewed'           => $query->row['viewed']
			);
		} else {
			return false;
		}
	}


	public function getBillInfo(){
		$this->load->model("account/naxumcustomer");
		$customer_id = $this->model_account_naxumcustomer->getCurrentCustomerID();	
		
		$sql = "select * from billing where userid = :customerid";
		$result = $this->db->query($sql,array(":customerid"=>$customer_id));
		
		return $result->num_rows;
	}
	
	public function getTransactionHistory(){
		$this->load->model("account/naxumcustomer");
		$customer_id = $this->model_account_naxumcustomer->getCurrentCustomerID();	
		
		$sql = "SELECT och.*,u.fname,u.lname,(select count(*) from oc_autoship_historydetails ochd where ochd.autoshiptransid = och.id ) itemscount  from oc_autoship_history och left join oc_autoship oa on oa.id = och.autoshipid left join users u on u.id = oa.customer_id where oa.customer_id = :customerid";
		$result = $this->db->query($sql,array(":customerid"=>$customer_id));
		
		$arr = array();
		
		foreach ($result->rows as $ky => $val) {			
				$arr[$ky] = $val;
				$arr[$ky]['link'] = str_replace('&amp;', '&', $this->url->link('extension/module/autoship/receipt', 'id=' . $val['id'] ));
			}
		
		return $arr;
	
	}
	
	public function getProducts($data = array()) {
		$sql = "SELECT p.product_id, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND p.autoship = 1 AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special";

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " FROM " . DB_PREFIX . "category_path cp LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (cp.category_id = p2c.category_id)";
			} else {
				$sql .= " FROM " . DB_PREFIX . "product_to_category p2c";
			}

			if (!empty($data['filter_filter'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_filter pf ON (p2c.product_id = pf.product_id) LEFT JOIN " . DB_PREFIX . "product p ON (pf.product_id = p.product_id)";
			} else {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product p ON (p2c.product_id = p.product_id)";
			}
		} else {
			$sql .= " FROM " . DB_PREFIX . "product p";
		}

		$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.autoship = 1  AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

		if (!empty($data['filter_category_id'])) {
			if (!empty($data['filter_sub_category'])) {
				$sql .= " AND cp.path_id = '" . (int)$data['filter_category_id'] . "'";
			} else {
				$sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";
			}

			if (!empty($data['filter_filter'])) {
				$implode = array();

				$filters = explode(',', $data['filter_filter']);

				foreach ($filters as $filter_id) {
					$implode[] = (int)$filter_id;
				}

				$sql .= " AND pf.filter_id IN (" . implode(',', $implode) . ")";
			}
		}

		if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
			$sql .= " AND (";

			if (!empty($data['filter_name'])) {
				$implode = array();

				$words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_name'])));

				foreach ($words as $word) {
					$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
				}

				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}

				if (!empty($data['filter_description'])) {
					$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
				}
			}

			if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
				$sql .= " OR ";
			}

			if (!empty($data['filter_tag'])) {
				$implode = array();

				$words = explode(' ', trim(preg_replace('/\s+/', ' ', $data['filter_tag'])));

				foreach ($words as $word) {
					$implode[] = "pd.tag LIKE '%" . $this->db->escape($word) . "%'";
				}

				if ($implode) {
					$sql .= " " . implode(" AND ", $implode) . "";
				}
			}

			if (!empty($data['filter_name'])) {
				$sql .= " OR LCASE(p.model) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.sku) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.upc) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.ean) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.jan) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.isbn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
				$sql .= " OR LCASE(p.mpn) = '" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "'";
			}

			$sql .= ")";
		}

		if (!empty($data['filter_manufacturer_id'])) {
			$sql .= " AND p.manufacturer_id = '" . (int)$data['filter_manufacturer_id'] . "'";
		}

		$sql .= " GROUP BY p.product_id";

		$sort_data = array(
			'pd.name',
			'p.model',
			'p.quantity',
			'p.price',
			'rating',
			'p.sort_order',
			'p.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
				$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
			} elseif ($data['sort'] == 'p.price') {
				$sql .= " ORDER BY (CASE WHEN special IS NOT NULL THEN special WHEN discount IS NOT NULL THEN discount ELSE p.price END)";
			} else {
				$sql .= " ORDER BY " . $data['sort'];
			}
		} else {
			$sql .= " ORDER BY p.sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC, LCASE(pd.name) DESC";
		} else {
			$sql .= " ASC, LCASE(pd.name) ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$product_data = array();

		//print $sql;

		$query = $this->db->query($sql);

		foreach ($query->rows as $result) {
			$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
		}

		return $product_data;
	}

	public function updateAutoshipDeliveryDate($data){
		ini_set("display_errors",1);
		error_reporting(E_ALL);
		$previousdeliverydate = $data['deliverydate'];
		$new_deliverydate = date("Y-m-d",$this->get_schedule_for_next_month($data['deliverydate']));
		if (isset($data['nextdeliverydate'])) {
			print "its set ****************************\n";
			$lastdeliverydate = $data['nextdeliverydate'];
		}
		else 
		{
			$lastdeliverydate = $previousdeliverydate;
		}
		//$lastdeliverydate = date("Y-m-d",$this->get_schedule_for_next_month($data['deliverydate']));
		
		$retval = $this->db->adaptor->query("UPDATE  oc_autoship SET deliverydate=:deliverydate, nextdeliverydate=:nextdeliverydate,lastdeliverydate= :lastdeliverydate WHERE  customer_id=:customer_id",array(":deliverydate"=>$new_deliverydate,":nextdeliverydate"=>$new_deliverydate,":customer_id"=>$data['customer_id'],":lastdeliverydate"=>$lastdeliverydate));
		print_r($retval);
				return $retval;	
	}
	
	public function updateAutoship($data){
        $logger = new Log("xen-ado-product-change.log");
		$this->load->model("account/naxumcustomer");
		$customer_id = $this->model_account_naxumcustomer->getCurrentCustomerID();

		$query = $this->db->adaptor->query("select * from oc_autoship where customer_id = :customer_id",array(":customer_id"=>$customer_id));
        
         $logger->write("============= update ADO START: ".$customer_id." ===========".$data['total']."====".$data['deliverydate']."=\n");

		if ($query->num_rows > 0) {
			$next = $data['deliverydate'];
			/*if($query->row['lastdeliverydate'] !==NULL){
				//$this->log->write("???");
				$this->log->write(date("Y-m-d",$this->get_schedule_for_next_month($data['deliverydate'])));
				$next = date("Y-m-d",$this->get_schedule_for_next_month($data['deliverydate']));
			}*/
			if($data['deliverydate'] =="noUpdate"){
				$retval = $this->db->adaptor->query("UPDATE  oc_autoship SET lastmodified=NOW(),total=:total,shipping_total = :stotal  WHERE  customer_id=:customer_id",array(":customer_id"=>$customer_id,":total"=>$data['total'],":stotal"=>$this->config->get("xen_regular_cost")));
				
				
			}else{
				$retval = $this->db->adaptor->query("UPDATE  oc_autoship SET deliverydate=:deliverydate,lastmodified=NOW(),total=:total,shipping_total = :stotal,nextdeliverydate =:nextdeliverydate WHERE  customer_id=:customer_id",array(":deliverydate"=>$data['deliverydate'],":customer_id"=>$customer_id,":total"=>$data['total'],":stotal"=>$this->config->get("xen_regular_cost"),":nextdeliverydate"=>$data['deliverydate']));	
			}
		
//nextDate"=>date( 'm-d-Y',strtotime($next)),
			$json = array("result_id"=>$query->row['id'],"result_text"=>"Successfully updated!!!!!","rows_affected"=>$retval->num_rows,"customerid"=>$customer_id,"query"=>"");	

			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($json));
			
		}
		else
		{
			$retval = $this->db->adaptor->query("insert into oc_autoship(deliverydate,lastmodified,customer_id,total,nextdeliverydate) values (:deliverydate,NOW(),:customer_id,:total,:nextdeliverydate)",array(":deliverydate"=>$data['deliverydate'],":customer_id"=>$customer_id,":total"=>$data['total'],":nextdeliverydate"=>$data['deliverydate']));
			if ($retval->num_rows > 0) {
					$query = $this->db->adaptor->query("select * from oc_autoship where customer_id = :customer_id",array(":customer_id"=>$customer_id));
					$json = array("result_id"=>$query->row['id']);		

					$this->response->addHeader('Content-Type: application/json');
					$this->response->setOutput(json_encode($json));					
			}
			
		}
        $logger->write("============= update ADO END: ".$customer_id." ===========".$data['total']."====".$data['deliverydate']."=\n");
	
	}
	
	
	
	public function checkIfProductExist($product_id) {
		
	}
	
	public function addProduct($data){
		
			$this->load->model("catalog/product");
			$product_info = $this->model_catalog_product->getProduct($data['product_id']);	

			$this->load->model("extension/module/autoship");
			$autoship_info = $this->getAutoship();
			
			$result = $this->db->adaptor->query("SELECT * FROM oc_autoshipdetails where product_id = :product_id and autoship_id = :autoship_id",array(":product_id"=>$data['product_id'],"autoship_id"=>$autoship_info['id']));


			if ($result->num_rows >0 ){
				$result = $this->db->adaptor->query("UPDATE oc_autoshipdetails SET  quantity = quantity+ :qty , total = (quantity * :price) WHERE  autoship_id = :autoship_id and product_id = :product_id",array(":autoship_id"=>$autoship_info['id'],":product_id"=>$data['product_id'],":qty"=>$data['quantity'],":price"=>$product_info['price']));			
				
				print json_encode(array('result'=>$result->num_rows,'name'=>$product_info['name']));

			} else {
				$total_amount = $product_info['price'] * $data['quantity'];
				$result = $this->db->adaptor->query("INSERT INTO oc_autoshipdetails (autoship_id,product_id,quantity,price,total) VALUES (:autoship_id,:product_id,:quantity,:price,:total)",array(":autoship_id"=>$autoship_info['id'],":product_id"=>$data['product_id'],":quantity"=>$data['quantity'],":price"=>$product_info['price'],":total"=>$total_amount));	
				print json_encode(array('result'=>$result->num_rows,'name'=>$product_info['name']));
			}				
		}
		
		

		
	public function refreshAutoshipSummary($data){
			$result1 = $this->getAutoshipDetailsSummary();	
			$result0 = $this->getAutoship();
			$tempCount =$result1['qty'];
			$tempTotal = $result1['total']+ $result0['shipping_total'];
            
			if($result1['qty'] == ""){
				$tempCount =0;
				$tempTotal = 0;
			}
            //UPDATE oc_autoship on recalculate total 
		    $result = $this->db->adaptor->query("UPDATE oc_autoship SET  total=:total WHERE  id =:autoship_id ",array(":autoship_id"=>$result0['id'],":total"=>$tempTotal));
			print json_encode(array('count'=> $tempCount,'total'=>$this->currency->format($tempTotal, $this->session->data['currency'])));
	}
	
	  
	public function updateAutoshipDetails($data){
		
        $logger = new Log("xen-ado-product-change.log");
		$this->load->model("account/naxumcustomer");
		$customer_id = $this->model_account_naxumcustomer->getCurrentCustomerID();
        
        
		$this->db->adaptor->query("DELETE FROM oc_autoshipdetails where autoship_id = :id",array(":id"=>$data['autoship_id']));
	    $logger->write("==update ADO details START: ".$customer_id." ==\n");
		for ($i=0;$i<count($data['product_idss']);$i++)
		{
		$logger->write(">>>update ADO details: ".$customer_id." ==========TOTAL:".$data['total_amounts'][$i]."====PRODID:".$data['product_idss'][$i]."====STATUS:".$data['status'][$i]."****AUTOSHIPID:".$data['autoship_id']."=====QUANTITY:".$data['quantities'][$i]."==\n");
			//if($data['total_amounts'][$i]!=0){
				$result = $this->db->adaptor->query("INSERT INTO oc_autoshipdetails (autoship_id,product_id,quantity,price,total,is_active) VALUES (:autoship_id,:product_id,:quantity,:price,:total,:status)",array(":autoship_id"=>$data['autoship_id'],":product_id"=>$data['product_idss'][$i],":quantity"=>$data['quantities'][$i],":price"=>$data['prices'][$i],":total"=>$data['total_amounts'][$i],":status"=>$data['status'][$i]));
			//}			
			
		}
        $logger->write("==update ADO details END: ".$customer_id." ==\n");
		
 
	}
	
	public function getAutoship(){

		$this->load->model("account/naxumcustomer");
		$customer_id = $this->model_account_naxumcustomer->getCurrentCustomerID();		
		$result = $this->db->adaptor->query("SELECT * FROM oc_autoship where customer_id = :id",array(":id"=>$customer_id));
		
		if ($result->num_rows > 0) {		
			$the_result =  $result->row;
			
			//$the_result['nextdeliverydate'] = date("Y-m-d",$this->get_schedule_for_next_month($the_result['deliverydate']));
			return $the_result;
		}
		else {
			return array('is_active'=>0);
		}
	
	}

	public function getAutoshipDetails(){	

		$this->load->model("account/naxumcustomer");
		$customer_id = $this->model_account_naxumcustomer->getCurrentCustomerID();		
		$result = $this->db->adaptor->query("SELECT * FROM oc_autoship where customer_id = :id",array(":id"=>$customer_id));
		$rows = [];
		
		//$rows = ["is_active"=>False];		
		
		
		if ($result->num_rows > 0  ) {
			$autoship_id = $result->row['id'];
		
			$result2 = $this->db->adaptor->query("SELECT od.*,op.model as name FROM oc_autoshipdetails od LEFT JOIN oc_product op  on op.product_id = od.product_id WHERE autoship_id = :id",array(":id"=>$autoship_id));
			

			foreach ($result2->rows as $the_row) {
				$the_row['fprice'] =  $this->currency->format($the_row['price'], $this->session->data['currency']);
				$the_row['ftotal'] =  $this->currency->format($the_row['total'], $this->session->data['currency']);
				$rows[] = $the_row;
				
			}
		}
		return $rows;
	
	}
	

	public function getAutoshipDetailsSummary(){	

		$this->load->model("account/naxumcustomer");
		$customer_id = $this->model_account_naxumcustomer->getCurrentCustomerID();		
		$result = $this->db->adaptor->query("SELECT * FROM oc_autoship where customer_id = :id",array(":id"=>$customer_id));
		$rows = [];
		
		//$rows = ["is_active"=>False];		
		
		
		if ($result->num_rows > 0  ) {
			$autoship_id = $result->row['id'];
		
			$result2 = $this->db->adaptor->query(" select sum(quantity) qty ,sum(total) total  from oc_autoshipdetails  where  is_active=1 and autoship_id = :id",array(":id"=>$autoship_id));
			

			return $result2->row;
				
			}
		
		return $rows;
	
	}
	
	
	public function processPayment($auth_info){
		
		$this->load->library("naxumcart");
        $logger = new Log("xen-autoship.log");
		if ($auth_info['billmethod'] == 'CC') {
				$res = $this->naxumcart->payingAutoshipCC($auth_info);
				if ($res['declined']) {					
				
					$this->load->model("account/naxumcustomer");
					$customer_info = $this->model_account_naxumcustomer->getNaxumCustomer($auth_info['customer_id']);
					$billinginfo = $this->naxumcart->getBillingInformation($auth_info['customer_id']);
					
					$data_for_mail['customer_name'] = $billinginfo['billfname']." ".$billinginfo['billlname'];

					ob_start();
					@$ccnumber = system ( "/usr/bin/perl /var/rep/code/office/crypt.pl ".$billinginfo['ccnumber'] );
					ob_clean();
					$data_for_mail['info_try_cc'] = null;
					$data_for_mail['credit_card_digits4'] = "************".substr($ccnumber,-4);
					$data_for_mail['billing_info_site'] = "https://office.xenestalife.com/bi.cgi?p=1";
					$data_for_mail['site_name'] =  $customer_info['site'];
					$data_for_mail['error_message_card'] = $res['error_text'];
					
					
					$html_version = $this->load->view("mail/autoship_decline",$data_for_mail);
					
					$text_version = $this->parser->parseString($html_version);

					$subject = "We Couldn't Process Your Xenesta ADO Payment";

					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

					$mail->setTo($customer_info['email']);
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender(html_entity_decode('Xenesta', ENT_QUOTES, 'UTF-8'));
					$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
					$mail->setHtml($html_version);
					$mail->setText($text_version);
					$mail->send();					
				}
				
				// automatically deactivate autoship onced failed
				if (!$res['approved']) { 
					$sql = "UPDATE oc_autoship set  is_active = 0  where  customer_id = :userid";	
					$this->db->adaptor->query($sql,array(':userid'=>$auth_info['customer_id']));					
					
				}
				
				
			}
			else {
				
                
				print "Ledger account processing  UPDATE\n";
				$res = $this->processPaymentLedger($auth_info);
                
                if ($res['approved'] == 0) {

                    print "Ledger account DECLINED !\n";
                    $logger->write(">>send declined info for ledger accounts \n");
                    $this->load->model("account/naxumcustomer");
					$customer_info = $this->model_account_naxumcustomer->getNaxumCustomer($auth_info['customer_id']);
					$billinginfo = $this->naxumcart->getBillingInformation($auth_info['customer_id']);
                    
                    ob_start();
					@$ccnumber = system ( "/usr/bin/perl /var/rep/code/office/crypt.pl ".$billinginfo['ccnumber'] );
					ob_clean();
                    
                    //BILL USER USING CC IF USER HAS CC
                    if((!is_null($ccnumber) && $ccnumber.trim() != "") && (!is_null($billinginfo['ccexp']) && $billinginfo['ccexp'].trim() != "") ){
                            $logger->write(">> TRY CC Account instead of ledger \n");
                            $res = $this->naxumcart->payingAutoshipCC($auth_info);
                            $res['try_cc_info']=1;
                            if ($res['declined']) {	
                                 $logger->write(">> TRY CC Account instead of ledger ,result:FAILED\n");
                                 $data_for_mail['credit_card_digits4'] = "************".substr($ccnumber,-4);
                                    $data_for_mail['customer_name'] = $billinginfo['billfname']." ".$billinginfo['billlname'];
                                    $data_for_mail['billing_info_site'] = "https://office.xenestalife.com/bi.cgi?p=1";
                                    $data_for_mail['site_name'] =  $customer_info['site'];
                                    $data_for_mail['error_message_card'] = $res['error_text'];
                                    $data_for_mail['info_try_cc']= '<br><br>You are using <b>LEDGER</b> as your billmethod but currently it does not have enough balance for this ADO. The <b>Credit Card</b> on your billing info is used instead.You may choose to UPDATE your LEDGER account or UPDATE your billmethod to Credit Card for your next ADO.<br><br>';
                                    
                                    
                                    $html_version = $this->load->view("mail/autoship_decline",$data_for_mail);
                                    
                                    $text_version = $this->parser->parseString($html_version);

                                    $subject = "We Couldn't Process Your Xenesta ADO Payment";

                                    $mail = new Mail();
                                    $mail->protocol = $this->config->get('config_mail_protocol');
                                    $mail->parameter = $this->config->get('config_mail_parameter');
                                    $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                                    $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                                    $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                                    $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                                    $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

                                    $mail->setTo($customer_info['email']);
                                    $mail->setFrom($this->config->get('config_email'));
                                    $mail->setSender(html_entity_decode('Xenesta', ENT_QUOTES, 'UTF-8'));
                                    $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
                                    $mail->setHtml($html_version);
                                    $mail->setText($text_version);
                                    $mail->send();	
                                    
                                     // automatically deactivate autoship onced failed
                                    if (!$res['approved']) { 
                                        $sql = "UPDATE oc_autoship set  is_active = 0  where  customer_id = :userid";	
                                        $this->db->adaptor->query($sql,array(':userid'=>$auth_info['customer_id']));					
                                        
                                    }
                             }
                                
                               
                                    
                                    
                            
                            
                        
                        
                    }else{//NO CC SEND DECLINED MESSAGE FOR LEDGER
                            $logger->write(">> NO CC info on BILLINFO ,proceed to sending declined email for ledger. \n");
                            $data_for_mail['customer_name'] = $billinginfo['billfname']." ".$billinginfo['billlname'];

                            $data_for_mail['billing_info_site'] = "https://office.xenestalife.com/bi.cgi?p=1";
                            $data_for_mail['site_name'] =  $customer_info['site'];
                            $data_for_mail['error_message_card'] = "Insufficient Funds";
                            
                            
                            $html_version = $this->load->view("mail/autoship_decline_ledger",$data_for_mail);
                            
                            $text_version = $this->parser->parseString($html_version);

                            $subject = "We Couldn't Process Your Xenesta ADO Payment";

                            $mail = new Mail();
                            $mail->protocol = $this->config->get('config_mail_protocol');
                            $mail->parameter = $this->config->get('config_mail_parameter');
                            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

                            $mail->setTo($customer_info['email']);
                            $mail->setFrom($this->config->get('config_email'));
                            $mail->setSender(html_entity_decode('Xenesta', ENT_QUOTES, 'UTF-8'));
                            $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
                            $mail->setHtml($html_version);
                            $mail->setText($text_version);
                            $mail->send();	
                            
                            
                            // DEACTIVATE AUTOSHIP
                            $sql = "UPDATE oc_autoship set  is_active = 0  where  customer_id = :userid";	
                            $this->db->adaptor->query($sql,array(':userid'=>$auth_info['customer_id']));
                            $logger->write(">> DONE send declined info for ledger accounts \n");			
                    }                    
					
				
                }             
			}
		return $res;
		
	}
	
	private function zerofill($mStretch, $iLength = 2){
		
			$sPrintfString = '%0' . (int)$iLength . 's';
			return sprintf($sPrintfString, $mStretch);
		}
			
	
	public function processPaymentLedger($data){
					$this->load->model('account/naxumcustomer');
					$naxumuser = $this->model_account_naxumcustomer->getCurrentLedgerBalance($data['customer_id']);
                    
                

					$new_naxum_info = array();
					if ($data['total'] <= $naxumuser['current_balance'] ) {
						$new_naxum_info['updated_balance'] = $naxumuser['current_balance'] - $data['total'];
						$new_naxum_info['balance'] = $naxumuser['current_balance'] - $data['total'];
						
						$this->model_account_naxumcustomer->updateLedgerCurrentBalance($data['customer_id'], -1 * $data['total']);
						

						$this->model_account_naxumcustomer->updateLedgerBalance(array(":user_id"=>$data['customer_id'],":notes"=>"Autoship Execution from Cart",":amount"=> -1*$data['total'],":balance"=>$naxumuser['current_balance'],":updated_balance"=>$new_naxum_info['updated_balance'],":transaction_id"=> 0,":ledger_request_id"=>"",":received_from_user_id"=>"",":transferred_to_user_id"=>"",":commission_period_ids"=>"",":status"=>"Completed"));	
						return array("approved"=>1,"transaction_id"=>0);
					} else {
						return array("approved"=>0);	
					}
		}
	
	
	
	public function insertProcessAuth2Trans($data) {
		
		$this->load->library("naxumcart");
		
		$sql = "INSERT INTO `transactions` (`userid`, `sponsorid`, `billfname`, `billlname`, `billaddress`, `billcity`, `billstate`, `billzip`, `billcountry`, `businessacct`, `billbusiness`, `billmethod`, `ccnumber`, `ccexp`, `ccid`, `bankname`, `acctype`, `aba`, `bankacctnum`, `dlnum`, `dlstate`, `ssn`, `invoice`, `authcode`, `refnum`, `amount`, `itemid`, `description`, `transactiondate`, `ip`, `merchaccount`, `credited`, `status`, `email`, `error`, `reconid`, `requesttoken`, `trackid`, `billdate`, `type`) values ( :userid, :sponsorid, :billfname, :billlname, :billaddress, :billcity, :billstate, :billzip, :billcountry, :businessacct, :billbusiness, :billmethod, :ccnumber, :ccexp, :ccid, :bankname, :acctype, :aba, :bankacctnum, :dlnum, :dlstate, :ssn, :invoice, :authcode, :refnum, :amount, :itemid, :description, NOW(), :ip, :merchaccount, NULL, :status, :email, :error, :reconid, :requesttoken, :trackid, :billdate, :type)";

		$billinfo = $this->naxumcart->getBillingInformation($data['customer_id']);
		
		#refnum is the reference number of the authorizenet
		#invoice is the autoship id number

		$res = $this->db->adaptor->query($sql,array( ':userid'=>$data['customer_id'], ':sponsorid'=>$billinfo['sponsorid'], ':billfname'=>$billinfo['billfname'], ':billlname'=>$billinfo['billlname'], ':billaddress'=>$billinfo['billaddress'], ':billcity'=>$billinfo['billcity'], ':billstate'=>$billinfo['billstate'], ':billzip'=>$billinfo['billzip'], ':billcountry'=>$billinfo['billcountry'], ':businessacct'=>$billinfo['billbusiness'], ':billbusiness'=>$billinfo['billbusiness'], ':billmethod'=>$billinfo['billmethod'], ':ccnumber'=>$billinfo['ccnumber'], ':ccexp'=>$billinfo['ccexp'], ':ccid'=>$billinfo['ccid'], ':bankname'=>$billinfo['bankname'], ':acctype'=>$billinfo['acctype'], ':aba'=>$billinfo['aba'], ':bankacctnum'=>$billinfo['bankacctnum'], ':dlnum'=>$billinfo['dlnum'], ':dlstate'=>$billinfo['dlstate'], ':ssn'=>$billinfo['ssn'], ':invoice'=>"OCA-".$this->zerofill($data['id'],4), ':authcode'=>'', ':refnum'=>$data['transaction_id'], ':amount'=>$data['total'], ':itemid'=>'', ':description'=>'Opencart Autoship Charging ', ':ip'=>'', ':merchaccount'=>'boa',  ':status'=>'Approved', ':email'=>$billinfo['email'], ':error'=>0, ':reconid'=>0, ':requesttoken'=>0, ':trackid'=>0, ':billdate'=>(new DateTime($data['deliverydate']))->format('d'), ':type'=>'product'));		
		
		//ADD INVOICE NO
		$last_id = $this->db->adaptor->getLastId();
		$sql = "UPDATE `transactions` set `invoice` = :invoice where id = :id";
        $res = $this->db->adaptor->query($sql,array(":invoice"=>'XENC'.$last_id,":id"=>$last_id));

		return $last_id;
		
	
	}

	public function insertProcessAuth2TransDeclined($data) {
		
		$this->load->library("naxumcart");
		$logger = new Log("xen-autoship.log");
        
		$sql = "INSERT INTO `transactions` (`userid`, `sponsorid`, `billfname`, `billlname`, `billaddress`, `billcity`, `billstate`, `billzip`, `billcountry`, `businessacct`, `billbusiness`, `billmethod`, `ccnumber`, `ccexp`, `ccid`, `bankname`, `acctype`, `aba`, `bankacctnum`, `dlnum`, `dlstate`, `ssn`, `invoice`, `authcode`, `refnum`, `amount`, `itemid`, `description`, `transactiondate`, `ip`, `merchaccount`, `credited`, `status`, `email`, `error`, `reconid`, `requesttoken`, `trackid`, `billdate`, `type`) values ( :userid, :sponsorid, :billfname, :billlname, :billaddress, :billcity, :billstate, :billzip, :billcountry, :businessacct, :billbusiness, :billmethod, :ccnumber, :ccexp, :ccid, :bankname, :acctype, :aba, :bankacctnum, :dlnum, :dlstate, :ssn, :invoice, :authcode, :refnum, :amount, :itemid, :description, NOW(), :ip, :merchaccount, NULL, :status, :email, :error, :reconid, :requesttoken, :trackid, :billdate, :type)";

		$billinfo = $this->naxumcart->getBillingInformation($data['customer_id']);
		
		$product_description = $this->naxumcart->getNames($data['id']);
		#refnum is the reference number of the authorizenet
		#invoice is the autoship id number

         if($data['try_cc_info'] ==1){
                 $logger->write("orig billmethod is LEDGER ,transacted through CC \n");
                 $billinfo['billmethod']='CC';
         }
		$res = $this->db->adaptor->query($sql,array( ':userid'=>$data['customer_id'], ':sponsorid'=>$billinfo['sponsorid'], ':billfname'=>$billinfo['billfname'], ':billlname'=>$billinfo['billlname'], ':billaddress'=>$billinfo['billaddress'], ':billcity'=>$billinfo['billcity'], ':billstate'=>$billinfo['billstate'], ':billzip'=>$billinfo['billzip'], ':billcountry'=>$billinfo['billcountry'], ':businessacct'=>$billinfo['billbusiness'], ':billbusiness'=>$billinfo['billbusiness'], ':billmethod'=>$billinfo['billmethod'], ':ccnumber'=>$billinfo['ccnumber'], ':ccexp'=>$billinfo['ccexp'], ':ccid'=>$billinfo['ccid'], ':bankname'=>$billinfo['bankname'], ':acctype'=>$billinfo['acctype'], ':aba'=>$billinfo['aba'], ':bankacctnum'=>$billinfo['bankacctnum'], ':dlnum'=>$billinfo['dlnum'], ':dlstate'=>$billinfo['dlstate'], ':ssn'=>$billinfo['ssn'], ':invoice'=>"OCA-".$this->zerofill($data['id'],4), ':authcode'=>'', ':refnum'=>$data['transaction_id'], ':amount'=>$data['total'], ':itemid'=>'', ':description'=>$product_description, ':ip'=>'', ':merchaccount'=>'boa',  ':status'=>'Declined', ':email'=>$billinfo['email'], ':error'=>$data['error_text'], ':reconid'=>0, ':requesttoken'=>0, ':trackid'=>0, ':billdate'=>(new DateTime($data['deliverydate']))->format('d'), ':type'=>'product'));		
		
		//ADD INVOICE NO
		$last_id = $this->db->adaptor->getLastId();
		$sql = "UPDATE `transactions` set `invoice` = :invoice where id = :id";
        $res = $this->db->adaptor->query($sql,array(":invoice"=>'XENC'.$last_id,":id"=>$last_id));

		return $last_id;
		
	
	}
	
	//AUTOSHIP SEND MAIL
	public function addAutoshipMail($autoship_id,$trans_id ,$isTryCC){
		//LOAD LANGUAGE
        $logger = new Log("xen-autoship.log");
		$language = new Language($this->config->get('config_language'));
		$language->load($this->config->get('config_language'));
		$language->load('mail/order');
		$this->load->model("account/naxumcustomer");
		//$customer_id = $this->model_account_naxumcustomer->getCurrentCustomerID();

		//QUERIES
		$autoship_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "autoshipdetails` WHERE is_active=1 and autoship_id = '" . (int)$autoship_id . "' ");
		$autoship1_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "autoship` WHERE id = '" . (int)$autoship_id . "' ");
		$customer_query = $this->db->query("SELECT CONCAT(fname,' ',lname) as `name`,`email`,`site` FROM `users` WHERE id = '" . (int)$autoship1_query->row['customer_id'] . "' ");
		$invoice_query = $this->db->query("SELECT invoice FROM `transactions` WHERE id = '" . (int)$trans_id . "' ");

		
		//SUBJECT
		$invoice = $invoice_query->row['invoice'];
		$subject = sprintf('Xenestalife  Recurring Billing Purchase Receipt ', html_entity_decode('', ENT_QUOTES, 'UTF-8'), $invoice);
		
		//HTML EMAIL
		$data = array();
		$data['products'] = array();
		$data['totals'] = array();
		
		//PRODUCTS
		$subtotal = 0;
		foreach ($autoship_query->rows as $product) {
				$prodName = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product` WHERE product_id = '" . (int)$product['product_id'] . "' ");
		
				$data['products'][] = array(
								'name'     => $prodName->row['model'],
								'quantity' => $product['quantity'],
								'price'    => $product['price'],
								'total'    => $product['total']
				);
				//$subtotal = $subtotal + (int)$product['total'];
		}
		
		//SHIPPIING FEE
		/*
		$this->load->model('extension/extension');

		$results = $this->model_extension_extension->getExtensions('shipping');
		$shippingCost = 0;
		$shippingText= "";
		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('extension/shipping/' . $result['code']);

				$quote = $this->{'model_extension_shipping_' . $result['code']}->getQuote($this->session->data['shipping_address']);

				if ($quote) {
					$shippingCost = $quote['quote']['cost'];
					$shippingText = $quote['quote']['text'];
				}
			}
		}*/
			
		//TOTALS
		$total = floatval($autoship1_query->row['total']);
		$shipping = floatval($autoship1_query->row['shipping_total']);
		$subtotal = floatval($total)- floatval($shipping);
		
		$data['totals'][] = array(
								'title'     => 'Sub-Total',
								'text' => $this->currency->format($subtotal,$this->session->data['currency'])
		);
		$data['totals'][] = array(
								'title'     => 'Shipping',
								'text' => $this->currency->format($shipping,$this->session->data['currency'])
		);
		$data['totals'][] = array(
								'title'     => 'Total',
								'text' => $this->currency->format($total,$this->session->data['currency'])
		);
		
		
		
		
		$data['title'] = sprintf('Xenestalife  Recurring Billing Purchase Receipt', 'Xenesta', $invoice);
	
		$data['text_greeting'] = sprintf($language->get('text_new_greeting'),'Xenesta');
        if($isTryCC){
            $data['try_cc_info']= '<br><br>You are using <b>LEDGER</b> as your billmethod but currently it does not have enough balance for this ADO. The <b>Credit Card</b> on your billing info is used instead.You may choose to UPDATE your LEDGER account or UPDATE your billmethod to Credit Card for your next ADO.<br><br>';
            $data['billing_info_site'] = "https://office.xenestalife.com/bi.cgi?p=1";
            $data['site_name']=$customer_query->row['site'];
        }else{
             $data['try_cc_info']= null;
             $data['billing_info_site']=null;
             $data['site_name'] = null;
        }
		$data['text_receipt']=  $language->get('text_new_receipt');
		$data['text_support']		=	$language->get('text_new_support');
		$data['text_close_sal']		= $language->get('text_new_close_sal');
		$data['text_from']				= $language->get('text_new_from');
		$data['text_link'] = $language->get('text_new_link');
		$data['text_download'] = $language->get('text_new_download');
		$data['text_order_detail'] = $language->get('text_new_order_detail');
		$data['text_instruction'] = $language->get('text_new_instruction');
		$data['text_order_id'] = 'Invoice No.';
		$data['text_date_added'] = $language->get('text_new_date_added');
		$data['text_payment_method'] = $language->get('text_new_payment_method');
		$data['text_shipping_method'] = $language->get('text_new_shipping_method');
		$data['text_email'] = $language->get('text_new_email');
		$data['text_telephone'] = $language->get('text_new_telephone');
		$data['text_ip'] = $language->get('text_new_ip');
		$data['text_order_status'] = $language->get('text_new_order_status');
		$data['text_payment_address'] = $language->get('text_new_payment_address');
		$data['text_shipping_address'] = $language->get('text_new_shipping_address');
		$data['text_product'] = $language->get('text_new_product');
		$data['text_model'] = $language->get('text_new_model');
		$data['text_quantity'] = $language->get('text_new_quantity');
		$data['text_price'] = $language->get('text_new_price');
		$data['text_total'] = $language->get('text_new_total');
		$data['text_footer'] = $language->get('text_new_footer');
		$data['text_semi_footer'] = $language->get('text_semi_footer');

		$data['logo'] = $this->config->get('config_url') . 'image/' . $this->config->get('config_logo');
		$data['store_name'] = 'Xenesta';
		$data['store_url'] ='https://office.xenestalife.com/cart/';
		$data['customer_id'] = $customer_query->row['name'];
		$data['invoice'] =$invoice;
				
		//SEND MAIL
		$text ="";
		$text .= $language->get('text_new_footer') . "\n\n";
	
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($customer_query->row['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode('Xenesta', ENT_QUOTES, 'UTF-8'));
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setHtml($this->load->view('mail/autoship', $data));
		$mail->setText($text);
		$mail->send();
        $logger->write("email sent to : ".$customer_query->row['email']."\n");
	}
	
	public function insert2ShoppingPurchases($data,$trans_id) {
		
		
		if($data['billmethod'] == "LEDGER"){
			        $sql = "UPDATE ledger_transaction_history SET transaction_id = :id WHERE user_id = :userId"; 
			        $this->db->adaptor->query($sql,array(":id"=>$trans_id,":userId"=>$data['customer_id']));
		
		}
		
		
		$sql = "INSERT INTO `shoppingcart_purchases` ( `userid`, `productid`, `quantity`, `purchasedate`, `billdate`, `enrollment`, `backoffice`, `recurperiod`, `active`, `lasttransdate`, `tranid`) VALUES ( :userid, :productid, :quantity, NOW(), NOW(), :enrollment, :backoffice, :recurperiod, :active, NOW(), :tranid)";
		
		$result = $this->db->adaptor->query("select ocd.*,oldp.old_id from oc_autoshipdetails ocd left join oc_oldproduct oldp on oldp.new_id = ocd.product_id where ocd.is_active =1 and ocd.autoship_id = :id",array(":id"=>$data['id']));
		
		foreach ($result->rows as $therow) {
			
				print "Printing the row ";
				print_r($therow);
		
				$res = $this->db->adaptor->query($sql,array(':userid'=>$data['customer_id'], ':productid'=>$therow['old_id'], ':quantity'=>$therow['quantity'],  ':enrollment'=>0, ':backoffice'=>0, ':recurperiod'=>'Month', ':active'=> 1,':tranid'=>$trans_id));
				print_r($res);
	
		}
	
	}
	
	
	public function activateAutoship($data){
		
		$result2 = $this->db->adaptor->query("SELECT * FROM oc_autoship WHERE customer_id = :id",array(":id"=>$_REQUEST['user_id']));
	
		$this->load->model("extension/module/autoship");
		$autoship_info = $this->getAutoship();
		
		if ($result2->num_rows == 0 ) {//SET NEXTDELIVERYDATE TO +1 MONTH FROM NOW UPON ACTIVATION
			$sql = "INSERT oc_autoship (customer_id,is_active,nextdeliverydate,deliverydate) values (:userid,:active_status,DATE_ADD(NOW(), INTERVAL 1 MONTH),DATE_ADD(NOW(), INTERVAL 1 MONTH))";		
			$this->db->adaptor->query($sql,array(":userid"=>$_REQUEST['user_id'],":active_status"=>$_REQUEST['active_status']));
		}
		else {
			
			
			$sql = "UPDATE oc_autoship set  is_active = :active_status, nextdeliverydate = DATE_ADD(NOW(), INTERVAL 1 MONTH) , deliverydate = DATE_ADD(NOW(), INTERVAL 1 MONTH) where  customer_id = :userid";		
			$this->db->adaptor->query($sql,array(":active_status"=>$_REQUEST['active_status'],":userid"=>$_REQUEST['user_id']));
			
			$sql = "UPDATE oc_autoshipdetails set  is_active = :active_status where autoship_id = :autoship_id";		
			$this->db->adaptor->query($sql,array(":active_status"=>$_REQUEST['active_status'],":autoship_id"=>$autoship_info['id']));

	
		}
		
		
	}
	
	public function updateProdStatus($data){
		
		$this->load->model("catalog/product");
		$product_info = $this->model_catalog_product->getProduct($data['product_id']);	

		$this->load->model("extension/module/autoship");
		$autoship_info = $this->getAutoship();
			

		$result = $this->db->adaptor->query("SELECT * FROM oc_autoshipdetails where product_id = :product_id and autoship_id = :autoship_id",array(":product_id"=>$data['product_id'],"autoship_id"=>$autoship_info['id']));
		
		if ($result->num_rows >0 ){
				$result = $this->db->adaptor->query("UPDATE oc_autoshipdetails SET  is_active=:status WHERE  autoship_id = :autoship_id and product_id = :product_id",array(":status"=>$data['active_status'],":autoship_id"=>$autoship_info['id'],":product_id"=>$data['product_id']));			
				
				

		} 
		
	}
	public function processTransactions($data){
		$logger = new Log("xen-autoship.log");
		//GET DETAILS
		$result = $this->db->adaptor->query("select ocd.*,oldp.old_id from oc_autoshipdetails ocd left join oc_oldproduct oldp on oldp.new_id = ocd.product_id where ocd.autoship_id = :id",array(":id"=>$data['id']));
		
		if((int)$data['total'] ==0 or is_null($data['total'])){
			//SUM
			$sumQuery = $this->db->adaptor->query("select SUM(total) as sum from oc_autoshipdetails ocd where ocd.autoship_id = :id",array(":id"=>$data['id']));
			$data['total']=floatval($sumQuery->row['sum'])+ floatval($this->config->get("xen_regular_cost"));
			$updateQuery = $this->db->adaptor->query("update oc_autoship set shipping_total=:stotal ,total=:total where id = :id",array(":id"=>$data['id'],":total"=>$data['total'],":stotal"=>$this->config->get("xen_regular_cost")));
			
		}
		


		$sql2 = "INSERT INTO oc_autoship_history (dateupdated,autoshipid) values (NOW(),:autoshipid)";
		$this->db->query($sql2,array(":autoshipid"=>$data['id']));
		$aut_trans_id =  $this->db->adaptor->getLastId();

		//TRANSACTION
		$trans_id = $this->insertProcessAuth2Trans($data);
		$data['trans_id'] = $trans_id;
		$prodDesc="";
        $logger->write("totalamount : ".$data['total']." \n");
        $logger->write("transaction id : ".$trans_id."\n");
		
					
		//INSERT TO SHOPPINGCART
		foreach ($result->rows as $therow) {
				

				$data['total_amount'] = $therow['total'];
				
				
				//UPDATE OR INSERT
				$existingShoppingPurch = $this->db->adaptor->query("select id from shoppingcart_purchases where userid= :id AND productid=:prodId",array(":id"=>$data['customer_id'],":prodId"=>$therow['old_id'] ));
				
				if($existingShoppingPurch->num_rows > 0){//UPDATE STATUS ON SHOPPINGCART
					$updateShoppPurch = "UPDATE shoppingcart_purchases set quantity = :quantity,purchasedate= NOW(),billdate=NOW(),active=:isActive,lasttransdate=NOW(), tranid = :tranid WHERE productid = :productid and userid = :userid  ";	
					
					$res = $this->db->adaptor->query($updateShoppPurch,array(':userid'=>$data['customer_id'], ':productid'=>$therow['old_id'], ':quantity'=>$therow['quantity'], ':tranid'=>$trans_id, ':isActive'=>$therow['is_active']));
				}else{
					if((int)$therow['is_active']== 1){//INSERT ONLY ACTIVE
						$insertShopPurch = "INSERT INTO `shoppingcart_purchases` ( `userid`, `productid`, `quantity`, `purchasedate`, `billdate`, `enrollment`, `backoffice`, `recurperiod`, `active`, `lasttransdate`, `tranid`) VALUES ( :userid, :productid, :quantity, NOW(), NOW(), :enrollment, :backoffice, :recurperiod, :active, NOW(), :tranid)";
			
						$res = $this->db->adaptor->query($insertShopPurch,array(':userid'=>$data['customer_id'], ':productid'=>$therow['old_id'], ':quantity'=>$therow['quantity'],  ':enrollment'=>0, ':backoffice'=>0, ':recurperiod'=>'Month', ':active'=> 1,':tranid'=>$trans_id));
					}

				}
				
				if((int)$therow['is_active']== 1){
					$prodDescQuery = $this->db->adaptor->query("select name from shoppingcart_products where id = :id",array(":id"=>$therow['old_id']));
					$this->log->write($prodDescQuery->row['name']);
					$prodDesc= $prodDesc.$prodDescQuery->row['name'].",";
				}
				$data['aut_trans_id'] = $aut_trans_id ;
				$this->updateTransactionHistoryDetails($data);
				
		}//END FOREACH		

		//UPDATE TRANSACTION DESCRIPTION
		
        if( $data['try_cc_info'] ==1){//if ledger billing account is ledger but is using CC for ADO
                $logger->write("orig billmethod is LEDGER ,transacted through CC \n");
                $updateTrans = "UPDATE transactions set description = :desc,billmethod= :billmethod  WHERE id = :tranid  ";	
                $res = $this->db->adaptor->query($updateTrans,array( ':tranid'=>$trans_id, ':desc'=>$prodDesc , ':billmethod'=> 'CC'));
            
        }else{
            $updateTrans = "UPDATE transactions set description = :desc  WHERE id = :tranid  ";	
            $res = $this->db->adaptor->query($updateTrans,array( ':tranid'=>$trans_id, ':desc'=>$prodDesc));
        }
		
		$sql2 = "UPDATE oc_autoship_history SET total = :total WHERE id = :id";		
		$this->db->query($sql2,array(":total"=>$data['total'],":id"=>$aut_trans_id));
		$data['total_amount'] =  $this->config->get("xen_regular_cost");
		//$trans_id = $this->insertProcessAuth2Trans($data);
		//$data['trans_id'] = $trans_id;
		//$this->updateTransactionHistoryDetails($data);
		
		//CALL EMAIL
		$this->log->write("halakadiha");
		$this->addAutoshipMail($data['id'],$trans_id, $data['try_cc_info']);
		
		

	}
	
	
	public function updateTransactionHistoryDetails($data) {
		$sql = "INSERT INTO oc_autoship_historydetails (transaction_id,dateupdated,autoshiptransid) values (:transaction_id,NOW(),:aut_trans_id)";
		$this->db->adaptor->query($sql,array(":transaction_id"=>$data['trans_id'],":aut_trans_id"=>$data['aut_trans_id']));
	}
	
    
    public function getAutoshipHistoryDetails($data){
        
        $sql = "select ocad.*,ocp.model  from  oc_autoshipdetails ocad left join oc_autoship oca  on oca.id = ocad.autoship_id  left join oc_autoship_history och on och.autoshipid = oca.id   left join oc_product ocp on ocp.product_id = ocad.product_id where och.id = :id";
        
        $result = $this->db->adaptor->query($sql,array(":id"=>$data['']));
        
    }
    
	
	public function getTodaysCron(){
		$result = $this->db->adaptor->query("select oaut.*,b.billmethod from oc_autoship oaut left join billing b on b.userid = oaut.customer_id WHERE day(deliverydate)= day(NOW())");
		return $result->rows;	
	} 
	
	public function processTodays_ScheduledAutoship(){
        $today = date("F j, Y, g:i a");
        $logger = new Log("xen-autoship.log");
        $logger->write("============= start autoship: ".$today." ================\n");
        
		$result = $this->db->adaptor->query("select oaut.*,b.billmethod from oc_autoship oaut left join billing b on b.userid = oaut.customer_id WHERE date(nextdeliverydate)= date(NOW()) and oaut.is_active = 1");
        
        
        //var_dump($result->rows);
        //$result = $this->db->adaptor->query("select oaut.*,b.billmethod from oc_autoship oaut left join billing b on b.userid = oaut.customer_id WHERE  ( oaut.id = 1346) or (oaut.id = 1380)");
        
        //$result = $this->db->adaptor->query("select oaut.*,b.billmethod from oc_autoship oaut left join billing b on b.userid = oaut.customer_id WHERE  ( oaut.id = 1380) ");
        
		$res = array();
		foreach ($result->rows as $the_row) {	
                $logger->write("******************************************************\n");
				$logger->write("autoship for customer : ".$the_row['customer_id']." bill method: ".$the_row['billmethod']."\n");
				$res = $this->processPayment($the_row);
				$logger->write("payment result : ".$res['approved']." \n");
                $the_row['try_cc_info'] =0;
                if($res['try_cc_info']){
                        $the_row['try_cc_info'] =1;
                }
				if ($res['approved']) {
					$the_row['transaction_id'] = $res['transaction_id'];
                   
					$this->updateAutoshipDeliveryDate($the_row);
					$this->processTransactions($the_row);
					
				}
				else
				{
					
					print "Failed transaction here\n";
					$the_row['transaction_id'] = $res['transaction_id'];
					$the_row['error_text'] = $res['error_text'];
					$this->insertProcessAuth2TransDeclined($the_row);
					
				}
                $logger->write("******************************************************\n");
		}
		$logger->write("============= end autoship: ".$today." ================\n");
	}
	
	
	#from https://www.jeffgeerling.com/blogs/jeff-geerling/php-calculating-monthly
	function get_schedule_for_next_month($start_date = FALSE) {
	  if ($start_date) {
		$now = strtotime($start_date); // Use supplied start date.
	  } else {
		$now = time(); // Use current time.
	  }

	  // Get the current month (as integer).
	  $current_month = date('n', $now);

	  // If the we're in Dec (12), set current month to Jan (1), add 1 to year.
	  if ($current_month == 12) {
		$next_month = 1;
		$plus_one_month = mktime(0, 0, 0, 1, date('d', $now), date('Y', $now) + 1);
	  }
	  // Otherwise, add a month to the next month and calculate the date.
	  else {
		$next_month = $current_month + 1;
		$plus_one_month = mktime(0, 0, 0, date('m', $now) + 1, date('d', $now), date('Y', $now));
	  }

	  $i = 1;
	  // Go back a day at a time until we get the last day next month.
	  while (date('n', $plus_one_month) != $next_month) {
		$plus_one_month = mktime(0, 0, 0, date('m', $now) + 1, date('d', $now) - $i, date('Y', $now));
		$i++;
	  }

	  return $plus_one_month;
	}
	
		//update delivery date when ado is re-activated
	public function updateDeliveryDate(){
		
		$this->log->write("update delivery");
		$this->load->model("account/naxumcustomer");
		$customer_id = $this->model_account_naxumcustomer->getCurrentCustomerID();
	
		$query = $this->db->adaptor->query("select * from oc_autoship where customer_id = :customer_id",array(":customer_id"=>$customer_id));
		
		
		if ($query->num_rows > 0) {

			
		
		
		
		
		
		/*
		
			$this->log->write("1");
            $result = $query->row;
			$delDate =date("Y-m-d",$result['nextdeliverydate']);
			$this->log->write("deldate");
			$this->log->write(date("Y-m-d",$result['nextdeliverydate']));
			
			$deliverydate=date("Y-m-d");//y-m-d
			$this->log->write("deliv");
			$this->log->write($deliverydate);
			if(strtotime($delDate)  > strtotime($deliverydate)){
				$this->log->write("2");
				$deliverydate= $delDate;
			}else{
				$deliverydate=date("Y-m-d",strtotime('+1 month'));
				$this->log->write("3");
			}
		
			$this->log->write($deliverydate);
			
			$retval = $this->db->adaptor->query("UPDATE  oc_autoship SET lastmodified=NOW(),nextdeliverydate = :nextdeliverydate WHERE  customer_id= :customer_id",array(":nextdeliverydate"=>$deliverydate,":customer_id"=>$customer_id));
			
			$this->log->write("=================================");*/
		}
	
	}


	

	


}
