<?php
class ModelExtensionModuleOfficeOrders extends Model {

	public function insertOrderstoTmpTrans($data) {
		
		$this->load->library("naxumcart");
		
		$sql = "INSERT INTO `oc_tmp_transactions` (`userid`, `sponsorid`, `billfname`, `billlname`, `billaddress`, `billcity`, `billstate`, `billzip`, `billcountry`, `businessacct`, `billbusiness`, `billmethod`, `ccnumber`, `ccexp`, `ccid`, `bankname`, `acctype`, `aba`, `bankacctnum`, `dlnum`, `dlstate`, `ssn`, `invoice`, `authcode`, `refnum`, `amount`, `itemid`, `description`, `transactiondate`, `ip`, `merchaccount`, `credited`, `status`, `email`, `error`, `reconid`, `requesttoken`, `trackid`, `billdate`, `type`) values ( :userid, :sponsorid, :billfname, :billlname, :billaddress, :billcity, :billstate, :billzip, :billcountry, :businessacct, :billbusiness, :billmethod, :ccnumber, :ccexp, :ccid, :bankname, :acctype, :aba, :bankacctnum, :dlnum, :dlstate, :ssn, :invoice, :authcode, :refnum, :amount, :itemid, :description, NOW(), :ip, :merchaccount, NULL, :status, :email, :error, :reconid, :requesttoken, :trackid, :billdate, :type)";

		$billinfo = $this->naxumcart->getBillingInformation($data['customer_id']);
		
		#refnum is the reference number of the authorizenet
		#invoice is the autoship id number

		$res = $this->db->adaptor->query($sql,array( ':userid'=>$data['customer_id'], ':sponsorid'=>$billinfo['sponsorid'], ':billfname'=>$billinfo['billfname'], ':billlname'=>$billinfo['billlname'], ':billaddress'=>$billinfo['billaddress'], ':billcity'=>$billinfo['billcity'], ':billstate'=>$billinfo['billstate'], ':billzip'=>$billinfo['billstate'], ':billcountry'=>$billinfo['billcountry'], ':businessacct'=>$billinfo['billbusiness'], ':billbusiness'=>$billinfo['billbusiness'], ':billmethod'=>$data['billmethod'], ':ccnumber'=>$billinfo['ccnumber'], ':ccexp'=>$billinfo['ccexp'], ':ccid'=>$billinfo['ccid'], ':bankname'=>$billinfo['bankname'], ':acctype'=>$billinfo['acctype'], ':aba'=>$billinfo['aba'], ':bankacctnum'=>$billinfo['bankacctnum'], ':dlnum'=>$billinfo['dlnum'], ':dlstate'=>$billinfo['dlstate'], ':ssn'=>$billinfo['ssn'], ':invoice'=>"OCA-".$this->zerofill($data['id'],4), ':authcode'=>'', ':refnum'=>$data['transaction_id'], ':amount'=>$data['total_amount'], ':itemid'=>'', ':description'=>'ShoppingCart (OC)Charging ', ':ip'=>'', ':merchaccount'=>'boa',  ':status'=>'Approved', ':email'=>$billinfo['email'], ':error'=>0, ':reconid'=>0, ':requesttoken'=>0, ':trackid'=>0, ':billdate'=>(new DateTime($data['deliverydate']))->format('d'), ':type'=>'product'));		
		
	
		return $this->db->adaptor->getLastId();
		
	
	}

	public function insertOrderstoTrans($data) { 
		
		$this->load->library("naxumcart");
		
        $order_id = $data['order_id'];
        
        $order_details = $this->getorderDetails($order_id);
        $ids = [];

        
		$sql = "INSERT INTO `transactions` (`userid`, `sponsorid`, `billfname`, `billlname`, `billaddress`, `billcity`, `billstate`, `billzip`, `billcountry`, `businessacct`, `billbusiness`, `billmethod`, `ccnumber`, `ccexp`, `ccid`, `bankname`, `acctype`, `aba`, `bankacctnum`, `dlnum`, `dlstate`, `ssn`, `invoice`, `authcode`, `refnum`, `amount`, `itemid`, `description`, `transactiondate`, `ip`, `merchaccount`, `credited`, `status`, `email`, `error`, `reconid`, `requesttoken`, `trackid`, `billdate`, `type`) values ( :userid, :sponsorid, :billfname, :billlname, :billaddress, :billcity, :billstate, :billzip, :billcountry, :businessacct, :billbusiness, :billmethod, :ccnumber, :ccexp, :ccid, :bankname, :acctype, :aba, :bankacctnum, :dlnum, :dlstate, :ssn, :invoice, :authcode, :refnum, :amount, :itemid, :description, NOW(), :ip, :merchaccount, NULL, :status, :email, :error, :reconid, :requesttoken, :trackid, :billdate, :type)";        
		
		$sql2 ="INSERT INTO `shoppingcart_purchases` (`userid`,`productid`,`quantity`,`purchasedate`,`billdate`,`enrollment`,`backoffice`,`recurperiod`,`active`,`lasttransdate`,`tranid`) values (:userid,:productid,:quantity, NOW(),CURDATE(),0,1,'',1,NOW(),:tranid)";
		
		$totalAmount=0.0;
		$prodDesc ="";
		$transId ="";
        foreach ($order_details as $data2 ) {
            $billinfo = $this->naxumcart->getBillingInformation($data['customer_id']);
            
            #refnum is the reference number of the authorizenet
            #invoice is the autoship id number

            //$special_description = preg_replace('/[^\p{L}\p{N}\s]/u', '', $data2['name2']);
			$totalAmount =$totalAmount + (float)$data2['total'];
			$prodDesc = $prodDesc.$data2['name2'].",";
			$transId= $data2['oid'];

           
		}	
		
		
		 //1 TRANSACTION ROW FOR ALL ITEMS
		 $res = $this->db->adaptor->query($sql,array( ':userid'=>$data['customer_id'], ':sponsorid'=>$billinfo['sponsorid'], ':billfname'=>$billinfo['billfname'], ':billlname'=>$billinfo['billlname'], ':billaddress'=>$billinfo['billaddress'], ':billcity'=>$billinfo['billcity'], ':billstate'=>$billinfo['billstate'], ':billzip'=>$billinfo['billzip'], ':billcountry'=>$billinfo['billcountry'], ':businessacct'=>$billinfo['billbusiness'], ':billbusiness'=>$billinfo['billbusiness'], ':billmethod'=>$data['billmethod'], ':ccnumber'=>$billinfo['ccnumber'], ':ccexp'=>$billinfo['ccexp'], ':ccid'=>$billinfo['ccid'], ':bankname'=>$billinfo['bankname'], ':acctype'=>$billinfo['acctype'], ':aba'=>$billinfo['aba'], ':bankacctnum'=>$billinfo['bankacctnum'], ':dlnum'=>$billinfo['dlnum'], ':dlstate'=>$billinfo['dlstate'], ':ssn'=>$billinfo['ssn'], ':invoice'=>"XENC".$this->zerofill($data['id'],4), ':authcode'=>$data['auth'], ':refnum'=>$this->session->data['refnum_xen'], ':amount'=>$data['overalltotal'], ':itemid'=>$transId, ':description'=>$prodDesc, ':ip'=>'', ':merchaccount'=>'boa',  ':status'=>'Approved', ':email'=>$billinfo['email'], ':error'=>0, ':reconid'=>0, ':requesttoken'=>0, ':trackid'=>0, ':billdate'=>'', ':type'=>'product'));		
         $last_id = $this->db->adaptor->getLastId();
         $ids[] = $last_id;
			
			
		//ADD TO SHOPPINGCART_PURCHASES 1 PER ITEM	
		$invoice_id = $ids[0];
		foreach ($order_details as $data2 ) {
			$special_description = preg_replace('/[^\p{L}\p{N}\s]/u', '', $data2['name2']);
			//FOR NON-RECURRIING PRODUCTS
			$ocOrderProduct =  $this->db->adaptor->query("select quantity from `oc_order_product`  inner join oc_oldproduct on product_id=new_id where order_id = :order_id and old_id=:product_id",array(":order_id"=>$order_id,":product_id"=>$data2['oid']));
			
			if($ocOrderProduct->num_rows){
				$res = $this->db->adaptor->query($sql2,array(':userid'=>$data['customer_id'],':productid'=>$data2['oid'],':quantity'=>$ocOrderProduct->row['quantity'], ':tranid'=>$invoice_id));
			}
        }
        
        
       //UPDATE INVOICE NO
        foreach ($ids as $id) {	
            $sql = "UPDATE `transactions` set `invoice` = :invoice where id = :id";
            $res = $this->db->adaptor->query($sql,array(":invoice"=>'XENC'.$invoice_id,":id"=>$id));
		}
        //UPDATE OC_ORDER INVOICE NO AND PREFIX
		$sql = "UPDATE `oc_order` set `invoice_no` = :invoice where order_id = :id";
        $res = $this->db->adaptor->query($sql,array(":invoice"=>(int)$invoice_id,":id"=>$order_id));
		
		return $last_id;
		
	
	}
    
    public function getorderDetails($order_id) {
        $sql = "select ordp.*,sp.name as name2,oldp.old_id as `oid` from `oc_order_product` ordp left join oc_oldproduct oldp on oldp.new_id = ordp.product_id left join shoppingcart_products sp on oldp.old_id = sp.id where ordp.order_id = :order_id";
        $res = $this->db->adaptor->query($sql,array(":order_id"=>$order_id));
        return $res->rows;        
        
    }
    
    
    
    
	public function insert2ShoppingPurchases($data,$trans_id) {
		
		
		if($data['billmethod'] == "LEDGER"){
			        $sql = "UPDATE ledger_transaction_history SET transaction_id = :id WHERE user_id = :userId"; 
			        $this->db->adaptor->query($sql,array(":id"=>$trans_id,":userId"=>$data['customer_id']));
		
		}
		
		
		$sql = "INSERT INTO `shoppingcart_purchases` ( `userid`, `productid`, `quantity`, `purchasedate`, `billdate`, `enrollment`, `backoffice`, `recurperiod`, `active`, `lasttransdate`, `tranid`) VALUES ( :userid, :productid, :quantity, NOW(), NOW(), :enrollment, :backoffice, :recurperiod, :active, NOW(), :tranid)";
		
		$result = $this->db->adaptor->query("select ocd.*,oldp.old_id from oc_autoshipdetails ocd left join oc_oldproduct oldp on oldp.new_id = ocd.product_id where ocd.is_active =1 and ocd.autoship_id = :id",array(":id"=>$data['id']));
		
		foreach ($result->rows as $therow) {			

				$res = $this->db->adaptor->query($sql,array(':userid'=>$data['customer_id'], ':productid'=>$therow['old_id'], ':quantity'=>$therow['quantity'],  ':enrollment'=>0, ':backoffice'=>0, ':recurperiod'=>0, ':active'=> 1,':tranid'=>$trans_id));
				print_r($res);
	
		}
	
	}    
    
    
    
	private function zerofill($mStretch, $iLength = 2){
		
			$sPrintfString = '%0' . (int)$iLength . 's';
			return sprintf($sPrintfString, $mStretch);
		}    
    
  }  
    
?>
