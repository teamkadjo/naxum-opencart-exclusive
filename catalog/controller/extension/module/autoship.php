<?php
class ControllerExtensionModuleAutoship extends Controller {
	public function index() {
		$this->load->language('extension/module/account');

		$data['heading_title'] = $this->language->get('heading_title');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['title'] = $this->document->getTitle();
		$data['header'] = $this->load->controller('common/header');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		
		$data['breadcrumbs'][] = array(
						'text' => 'ADO',
						'href' => $this->url->link('extension/module/autoship', '')
					);		
		$data['shipping_cost'] = $this->config->get("xen_regular_cost");
		$this->load->model('extension/module/autoship');		
		$data['autoship_info'] = $this->model_extension_module_autoship->getAutoship();
		//print_r($data['autoship_info']);
		$data['autoship_details'] = $this->model_extension_module_autoship->getAutoshipDetails();
		
		//print_r($data['autoship_details']);
		
		$data['has_bill'] = $this->model_extension_module_autoship->getBillInfo();
		$data['autoship_link_history'] = $this->url->link('extension/module/autoship/viewhistory','');

		$this->load->model("account/naxumcustomer");
		$data['customer_id'] = $this->model_account_naxumcustomer->getCurrentCustomerID();
		

		if (($data['autoship_info']) && ($data['autoship_info']['is_active'])) {		
			$this->response->setOutput($this->load->view('extension/module/autoship', $data));
		}
		else 
		{
			 $this->response->setOutput($this->load->view('extension/module/autoshipdeactivated', $data));	
		}	
	
	}
	
	
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('extension/module/autoship');

			$filter_data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 5
			);

			$results = $this->model_extension_module_autoship->getProducts($filter_data);

			foreach ($results as $result) {
				$json[] = array(
					'product_id' => $result['product_id'],
					'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'model' => $result['model'],
					'price' => $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']),
					'link' => str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $result['product_id'] ))
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}	
	
	 
	public function updateAutoship(){

		$this->load->model("extension/module/autoship");
		$this->model_extension_module_autoship->updateAutoship($_REQUEST);
		//$this->model_extension_module_autoship->processTodays_ScheduledAutoship();
	}

	public function updateAutoshipDetails(){

		$this->load->model("extension/module/autoship");
		$this->model_extension_module_autoship->updateAutoshipDetails($_REQUEST);
	}

	public function addProduct(){

		$this->load->model("extension/module/autoship");
		$this->model_extension_module_autoship->addProduct($_REQUEST);
	}
	
	public function refreshAutoshipSummary(){
		$this->load->model("extension/module/autoship");
		$this->model_extension_module_autoship->refreshAutoshipSummary();		
	}	
	
	public function activateAutoship(){
		ini_set("display_errors",1);
		//error_reporting(E_ALL);
		$this->load->model("extension/module/autoship");
		$this->model_extension_module_autoship->activateAutoship($_REQUEST);		
	}
	
	public function updateProdStatus(){
		ini_set("display_errors",1);
		//error_reporting(E_ALL);
		$this->load->model("extension/module/autoship");
		$this->model_extension_module_autoship->updateProdStatus($_REQUEST);		
	}
	
	
	
	public function viewhistory(){
		$this->load->language('extension/module/account');

		$data['heading_title'] = $this->language->get('heading_title');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		

		$data['breadcrumbs'][] = array(
						'text' => 'Autoship',
						'href' => $this->url->link('extension/module/autoship', '')
					);		
		$data['shipping_cost'] = $this->config->get("xen_regular_cost");
		$this->load->model('extension/module/autoship');		
		//$data['autoship_info'] = $this->getHistory();
      
		$data['autoship_details'] = $this->model_extension_module_autoship->getAutoshipDetails();
		$data['autoship_history'] = $this->model_extension_module_autoship->getTransactionHistory();	
		$data['autoship_link_history'] = str_replace('&amp;', '&', $this->url->link('extension/module/autoship/viewhistory', ''));;	
		$data['bi_link'] = "/bi.cgi";
		$data['success']	 = '';
		$data['error_warning']	 = '';
		
		$this->response->setOutput($this->load->view('extension/module/autoship_history', $data));	
		
	
	}
    
  
    
    

    public function getAutoshipRecord($data){
        $authship_record = $this->db->adaptor->query("select oa.*,users.fname,users.lname  from oc_autoship oa   left join users on oa.customer_id = users.id where oa.id = :id",array(":id"=>$data['id']));
        return $authship_record->row;
    }

    public function getAutoshipRecordHistory($data){
        $authship_record = $this->db->adaptor->query("select oca.*,och.dateupdated from  oc_autoship oca   left join oc_autoship_history och on och.autoshipid = oca.id   where och.id = :id",array(":id"=>$data['id']));
        return $authship_record->row;
    }

    public function getAutoshipRecordDetailsHistory($data){
        $authship_record = $this->db->adaptor->query("select ocad.*,ocp.model,ocpd.name  from  oc_autoshipdetails ocad left join oc_autoship oca  on oca.id = ocad.autoship_id  left join oc_autoship_history och on och.autoshipid = oca.id   left join oc_product ocp on ocp.product_id = ocad.product_id left join oc_product_description ocpd on ocpd.product_id = ocp.product_id where och.id = :id",array(":id"=>$data['id']));
        return $authship_record->rows;
    }
    
    
    public function getTransactionsInfo($data){
        $info = $this->db->adaptor->query("select transaction_id  from oc_autoship_historydetails where autoshiptransid = :id",array(":id"=>$data['id']));
        if (count($info->rows) > 0) {
            $the_transaction_id = $info->row['transaction_id'];
            $trans_info = $this->db->adaptor->query("select *  from transactions where id = :id",array(":id"=>$the_transaction_id));


ini_set("display_errors",1);
error_reporting(E_ALL);            
            
			ob_start();
			@$ccnumber = system ( "/usr/bin/perl /var/rep/code/office/crypt.pl ".$trans_info->row['ccnumber'] );
			ob_clean();            
            
            $last4number = (strlen($ccnumber)>4)?substr($ccnumber, -4):$ccnumber;
            
            $trans_info->row['last4number'] = $last4number;
            return $trans_info->row;
        }
    }


	public function receipt(){

		$this->load->language('extension/module/account');

		$data['heading_title'] = $this->language->get('heading_title');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		

        
		$data['breadcrumbs'][] = array(
						'text' => 'Autoship',
						'href' => $this->url->link('extension/module/autoship', '')
					);		
		$data['shipping_cost'] = $this->config->get("xen_regular_cost");
		$this->load->model('extension/module/autoship');		
		$data['autoship_info'] = $this->getAutoshipRecordHistory($this->request->get);
        $data['id'] = $this->request->get['id'];
       
        
        
		$data['autoship_details'] = $this->getAutoshipRecordDetailsHistory($this->request->get);
        
        $data['trans_info'] = $this->getTransactionsInfo($this->request->get);
			
           
			
		$this->response->setOutput($this->load->view('extension/module/autoship_receipt', $data));	
	
	}
	
	
	
	public function cron(){
        
        if ($_REQUEST['hash']=="4007242891") {
				$this->load->model("extension/module/autoship");
		
                        if (function_exists('oc_cli_output')) {
                                oc_cli_output("Start cron..\n");
                        }
                        else {
                                echo "Start cron..\n";
                        }


	
				$this->model_extension_module_autoship->processTodays_ScheduledAutoship();
				
				//oc_cli_output("Done executing..");
			if (function_exists('oc_cli_output')) {	
				oc_cli_output("Done executing..\n");
			}
			else {
				echo "finished execution...\n";
			}
	    }
	}
	
	
		public function calculate_next(){
			error_reporting(0);
			echo "\n";
			$rawDate = "2017-3-9";
			//echo date('d', strtotime($rawDate));			
			$this->load->model("extension/module/autoship");			
			echo date("Y-m-d",$this->model_extension_module_autoship->jjg_calculate_next_month($rawDate));
			echo "\n";
			
		}
	
	
	
	
	
}
