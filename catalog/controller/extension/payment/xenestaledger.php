<?php
class ControllerExtensionPaymentXenestaledger extends Controller {
	public function index() {
		$data['button_confirm'] = $this->language->get('button_confirm');

		$data['text_loading'] = $this->language->get('text_loading');

		$data['continue'] = $this->url->link('checkout/success');	



		return $this->load->view('extension/payment/xenestaledger', $data);
	}

	public function confirm() {
		if ($this->session->data['payment_method']['code'] == 'xenestaledger') {
			
	if (isset($this->session->data['order_id'])) {
					$this->load->model('checkout/order');		
					$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
					
					$this->load->model('account/naxumcustomer');
					$naxumuser = $this->model_account_naxumcustomer->getNaxumUserInfo($order_info['customer_id']);

					//$the_array = array(':userid'=>$order_info['customer_id'],':sponsorid'=>$naxumuser['sponsorid'],':billfname'=>$naxumuser['fname'],':billlname'=>$naxumuser['lname'],':billaddress'=>$naxumuser['address'],':billcity'=>$naxumuser['city'],':billstate'=>$naxumuser['state'],':billcountry'=>$naxumuser['country'],':amount'=>$order_info['total'],':itemid'=>$order_info['order_id'],':description'=>'Opencart Order Number '.$order_info['order_id'],':is_rank_processed'=>'0',':new'=>'0',':inclannualfee'=>0,':inclsetupfee'=>0);					
					//$transid = $this->model_account_naxumcustomer->insertTransaction($the_array);


					$this->load->model('checkout/order');
					$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);


					$this->load->model('account/naxumcustomer');
					$naxumuser = $this->model_account_naxumcustomer->getCurrentLedgerBalance($order_info['customer_id']);

					$new_naxum_info = array();
					$new_naxum_info['updated_balance'] = $naxumuser['current_balance'] - $order_info['total'];
					$new_naxum_info['balance'] = $naxumuser['current_balance'] - $order_info['total'];
					
					$this->model_account_naxumcustomer->updateLedgerCurrentBalance($order_info['customer_id'], -1 * $order_info['total']);
					

					$this->model_account_naxumcustomer->updateLedgerBalance(array(":user_id"=>$order_info['customer_id'],":notes"=>"Purchase from Cart",":amount"=> -1*$order_info['total'],":balance"=>$naxumuser['current_balance'],":updated_balance"=>$new_naxum_info['updated_balance'],":transaction_id"=> $transid,":ledger_request_id"=>"",":received_from_user_id"=>"",":transferred_to_user_id"=>"",":commission_period_ids"=>"",":status"=>"Completed"));					

				}			
		

			$this->load->model('checkout/order');
			//$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('xenestaledger_order_status_id'));
		}
	}
}

