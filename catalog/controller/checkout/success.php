<?php
class ControllerCheckoutSuccess extends Controller {
	public function index() {
		$this->load->language('checkout/success');

		if (isset($this->session->data['order_id'])) {
 
          if (! $this->session->data['authcode_trans']) {
	            $auth = $this->session->data['authcode_trans'];
          }else{
              $auth ="";
          }



            //$this->log->write("====================");
            //$this->log->write($auth);
            // ADD NI DARL
            
            $order_data = array();

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array.
			$total_data = array(
				'totals' => &$totals,
				'taxes'  => &$taxes,
				'total'  => &$total
			);

			$this->load->model('extension/extension');

			$sort_order = array();

			$results = $this->model_extension_extension->getExtensions('total');

			foreach ($results as $key => $value) {
				$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
			}

			array_multisort($sort_order, SORT_ASC, $results);

			foreach ($results as $result) {
				if ($this->config->get($result['code'] . '_status')) {
					$this->load->model('extension/total/' . $result['code']);

					// We have to put the totals in an array so that they pass by reference.
					$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
				}
			}

			$sort_order = array();

			foreach ($totals as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $totals);
               $overalltotal=0.0;
			$order_data['totals'] = $totals;
            foreach ($order_data['totals'] as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text'  => $this->currency->format($total['value'], $this->session->data['currency'])
				);
                if($total['title'] =='Total'){
                    $overalltotal=floatval($total['value']);
                }
			}
         
            //CALL TRANSACTIONS HERE
			if (isset($this->session->data['order_id'])) {
                $order_id = $this->session->data['order_id'];
                $this->load->model("checkout/order");
                $data2 = $this->model_checkout_order->getOrder($order_id);
                
                $data2['transaction_id'] =$order_id;// $data['order_id'];
                $data2['id'] = $order_id;
                //$data2['total_amount'] = $data['total'];
                $data2['deliverydate'] = '';
                $data2['overalltotal']= $overalltotal;

                if ($data2['payment_code']=='xenestacard' or $data2['payment_code'] == 'authorizenet_aim') {
                    $data2['billmethod'] = 'CC';
                } elseif ($data2['payment_code'] == 'xenestaledger') {
                        $data2['billmethod'] = 'LEDGER';
                } else {
                            $data2['billmethod'] = '';
                }
                $data2['auth']=$auth;
                $this->load->model("extension/module/office_orders");                
                $recorded = $this->model_extension_module_office_orders->insertOrderstoTrans($data2);
			}
			//END TRANSACTIONS HERE
            $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], 5);
            //UPDATE STATUS TO COMPLTE
            $this->db->query("UPDATE " . DB_PREFIX . "order SET order_status_id = '5' WHERE order_id = '" . (int)$this->session->data['order_id'] . "'");
   
			//OK DONE
            
            
            $this->cart->clear();

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				if ($this->customer->isLogged()) {
					$activity_data = array(
						'customer_id' => $this->customer->getId(),
						'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
						'order_id'    => $this->session->data['order_id']
					);

					$this->model_account_activity->addActivity('order_account', $activity_data);
				} else {
					$activity_data = array(
						'name'     => $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'],
						'order_id' => $this->session->data['order_id']
					);

					$this->model_account_activity->addActivity('order_guest', $activity_data);
				}
			}

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
		}

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_basket'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_checkout'),
			'href' => $this->url->link('checkout/checkout', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('checkout/success')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		if ($this->customer->isLogged()) {
			//account options are hidden
			//$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', true), $this->url->link('account/order', '', true), $this->url->link('account/download', '', true), $this->url->link('information/contact'));
			$data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact')); 
		} else {
			$data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
		}

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/success', $data));
	}
}
