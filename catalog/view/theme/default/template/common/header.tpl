<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>

<link href="/css/systems/xen/menu.css?session-63572862262753" rel="stylesheet">
<link href="/css/systems/xen/menu-custom.css?session-63572862262753" rel="stylesheet">
<link href="/css/global.min.css?session-63572862262753" rel="stylesheet">



<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<nav id="top">
<nav class="navbar navbar-default top-nav">
	<!-- INNER -->
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topnav-collapse" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="fa fa-bars"></span>
			</button>
			<a class="navbar-brand" href="/"><img class="" src="//office.xenestalife.gmb/images/XEN-headerLogo.png" style="height:35px;" alt="Xenestalife">
			</a>
		</div>



		<!-- Top Navigation Content -->
		<div class="collapse navbar-collapse" id="topnav-collapse">
			<ul class="nav navbar-nav">
				<li class="my-info">
					<a class="profile-pic" href="/profile.cgi#picture-sub"><img class="img-circle" src="//xenestalife.com/images/people/<?php echo $customer_id; ?>.jpg?session-69190199612324" alt="Hi <?php echo $customer_name; ?>" onerror="this.src='/images/default-profile.png?session-69190199612324';" /></a>
				</li>
				<li class="text-uppercase my-info"><a class="profile-top-nav" href="/profile.cgi">Hi <?php echo $customer_name; ?></a><a class="signout" href="/logout.cgi">Sign Out</a></li>
			</ul>
			<hr class="visible-xs menu-divider">
			<ul class="nav navbar-nav topnav-content">
				<li class="index-page"><a class="top-nav-main" data-id="index-page" href="/index.cgi"><i class="fa fa-home" aria-hidden="true">&nbsp;</i><span class="visible-xs">Home</span></a></li>
				<li class="profile-page"><a class="top-nav-main" data-id="profile-page" href="/contacts.cgi"><i class="fa fa-book">&nbsp;</i>Contacts</a></li>
				<li class="team-page"><a class="top-nav-main" data-id="links-page" href="/xen_links.cgi"><i class="fa fa-link" aria-hidden="true">&nbsp;</i>Links</a></li>
				<li class="create-page"><a class="top-nav-main" data-id="tools-page" href="/tools.cgi"><i class="fa fa-cube" aria-hidden="true">&nbsp;</i>Tools</a></li>
				<li class="training-page"><a class="top-nav-main" data-id="training-page" href="/xen_training.cgi"><i class="fa fa-magic" aria-hidden="true">&nbsp;</i>Training</a></li>
				<li class="reports-page"><a class="top-nav-main" data-id="reports-page" href="/reports.cgi?p=member_dashboard"><i class="fa fa-area-chart" aria-hidden="true">&nbsp;</i>Reports</a></li>
				<li class="shop-page"><a class="top-nav-main" data-id="shop-page" href="/shop.cgi"><i class="fa fa-magic" aria-hidden="true">&nbsp;</i>Shop</a></li>
				<li class="support-page"><a class="top-nav-main" data-id="support-page" href="/support.cgi"><i class="fa fa-life-ring" aria-hidden="true">&nbsp;</i>Support</a></li>

			</ul>
			
		</div>
		<!-- Top Navigation Content || END -->


	</div>

</nav>
<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div id="logo">
		&nbsp;
        </div>
      </div>
      <div class="col-sm-5"><?php echo $search; ?>
      </div>
      <div class="col-sm-3"><?php echo $cart; ?></div>
    </div>
  </div>
</header>
<?php if ($categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>
<?php } ?>
