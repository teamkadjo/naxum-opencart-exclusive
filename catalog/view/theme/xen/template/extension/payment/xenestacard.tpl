<form id="payment" class="form-horizontal">
  <fieldset>
    <legend><?php echo $text_credit_card; ?></legend>

    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-cc-number"><?php echo $entry_cc_number; ?></label>
      <div class="col-sm-10">
        <input type="text" name="cc_number" value="XXXXXXXXXXX<?php echo $lastfewnumber; ?>" placeholder="<?php echo $entry_cc_number; ?>" id="input-cc-number" class="form-control" disabled/>
      </div>
    </div>

    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-cc-expire-date"><?php echo $entry_cc_expire_date; ?></label>
      <div class="col-sm-10">
       <input type="text" name="cc_cvv2" value="" placeholder="<?php echo $entry_cc_expire_date1; ?>" id="input-ccexp" class="form-control"  disabled />
      </div>
    </div>

    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-cc-cvv2"><?php echo $entry_cc_cvv2; ?></label>
      <div class="col-sm-10">
        <input style="width:8%"  maxlength="4" type="text" name="cc_cvv2" value="" placeholder="<?php echo $entry_cc_cvv2_place; ?>" id="input-cc-cvv2" class="form-control" />
      </div>
    </div>

  </fieldset>
</form>
<div class="buttons">
  <div class="pull-right">
    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="btn btn-primary" />
  </div>
</div>

<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.numeric.min.js"></script>

<script type="text/javascript"><!--

$(document).ready(function(){


$("#input-cc-cvv2").numeric();


});


$('#button-confirm').on('click', function() {
    var cvv2 = $('#input-cc-cvv2').val().trim();
    
    if( cvv2==""){
        alert("Card Security Code (CVV2) is required.");
    }
    if(cvv2 != "" ){
        $.ajax({
            url: 'index.php?route=extension/payment/xenestacard/send',
            type: 'post',
            data: $('#payment :input'),
            dataType: 'json',
            cache: false,
            beforeSend: function() {
                $('#button-confirm').button('loading');
            },
            complete: function() {
                $('#button-confirm').button('reset');
            },
            success: function(json) {
                if (json['error']) {
                    alert(json['error']);
                }

                if (json['redirect']) {
                    location = json['redirect'];
                }
            }
        });
    }
});
//--></script>
