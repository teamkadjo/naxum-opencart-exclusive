<?php echo $header; ?>
<link href="admin/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="/js/sweetalert.js"></script>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>

<?php if ($has_bill) { ?>

<div id="content" class="col-sm-9">      <h1>AUTO-DELIVERY ORDER PROFILE (ADO)</h1>
      <form id="theform"  method="post" enctype="multipart/form-data">
        <div class="table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <td class="text-left">Product Name</td>
                <td class="text-left">Quantity</td>
				<td class="text-left">Status</td> 
                <td class="text-right">Unit Price</td>
                <td class="text-right">Total</td>
              </tr>
            </thead>
            
            <tbody id="gp-rows">              
            <?php if ($autoship_details) { ?>
           <?php foreach ($autoship_details as $od) { ?>
                <tr class="gp_row" id="gp-row-child-<?php echo $od['product_id']; ?>">  <td class="text-left"><?php echo $od['name']; ?></td>   <td class="text-left">  
				<div style="max-width: 200px;" class="input-group btn-block"> 
				
				
				<?php if($od['is_active'] == 1){?>
					<input type="text" class="form-control input-quantity-value" size="1" value="<?php echo $od['quantity']; ?>" name="quantity[<?php echo $od['product_id']; ?>]" id="quantity-<?php echo $od['product_id']; ?>">
				<?php }else{?>
					<input type="text" class="form-control input-quantity-value" size="1" value="<?php echo $od['quantity']; ?>" name="quantity[<?php echo $od['product_id']; ?>]" id="quantity-<?php echo $od['product_id']; ?>" disabled>
				<?php }?>
				
				
				
				<span class="input-group-btn">
				
				
					<?php if($od['is_active'] == 1){?>
								<button data-original-title="Update" class="btn btn-primary" title="" data-toggle="tooltip" onclick="updateItem('<?php echo $od['product_id']; ?>');" type="button"><i class="fa fa-refresh"></i></button>
								
								<button data-original-title="Deactivate" onclick="deactivateItem('<?php echo $od['product_id']; ?>');" class="btn btn-default" title="Deactivate" data-toggle="tooltip" type="button"><i class="fa fa-calendar-times-o"></i></button>
								
							
					<?php }else{?>
								<button data-original-title="Update" class="btn btn-primary" title="" data-toggle="tooltip" onclick="updateItem('<?php echo $od['product_id']; ?>');" type="button" disabled><i class="fa fa-refresh"></i></button>
								
								<button data-original-title="Activate" onclick="reactiveItem('<?php echo $od['product_id']; ?>');" class="btn btn-warning" title="Activate" data-toggle="tooltip" type="button"><i class="fa fa-check-circle"></i></button>
					<?php }?>
				
						
						<button data-original-title="Remove" onclick="removeItem('<?php echo $od['product_id']; ?>');" class="btn btn-danger" title="" data-toggle="tooltip" type="button" hidden="true"><i class="fa fa-times-circle"></i></button>
						
						
						
						


				</span>
				</div></td>  
				<td class="text-right">
					<?php if($od['is_active'] == 1){?>
						<label>Active</label>
					<?php }else{?>
						<label>In Active</label>
					<?php }?>
				</td> 
				<td class="text-right" ><?php echo $od['fprice']; ?></td> 

				<?php if($od['is_active'] == 1){?>
						<td class="text-right totalcell" id ="gp-td-child-<?php echo $od['product_id']; ?>-fprice"><?php echo $od['ftotal']; ?>  </td>
				<?php }else{?>
							<td class="text-right totalcell" id ="gp-td-child-<?php echo $od['product_id']; ?>-fprice">$0.00  </td>
							<td style="display:none;"><?php echo $od['ftotal']; ?> </td>
				<?php }?>
			
				
				
				</tr>
           <?php } } ?>
           
           
             </tbody>
             
            <tfoot>
                
                               <tr>
                <td class="text-left" colspan="2"><div class="btn-group btn-block ">
<a  class="btn btn-primary  btn-block add-ado-product">Add ADO Products</a>
        </div></td>

                <td class="text-right">&nbsp;</td>
                <td class="text-right">&nbsp;</td>
				<td class="text-right">&nbsp;</td>
              </tr>
            </tfoot>

          </table>
        </div>
      </form>
   
      <div class="row">
        <div class="col-sm-4 col-sm-offset-8">
          <table class="table table-bordered">
                        <tbody>
            <tr>
              <td class="text-right"><strong>Shipping Cost:</strong></td>
              <td class="text-right ship_total">$<?php echo $shipping_cost; ?></td>
            </tr>
                        <tr>
              <td class="text-right"><strong>Total:</strong></td>
              <td class="text-right the_overall_total">$0.00</td>
            </tr>
                      </tbody></table>
        </div>
      </div>

      </div>



<div class="col-sm-3">
<div class="form-group">
              <label class="control-label" for="input-option227">Change Next Auto-Delivery Order Date:</label>
              <div class="input-group date">
                <input type="text" name="option[227]" placeholder="Change Delivery Date Here"  value="<?php echo (new DateTime($autoship_info['deliverydate']))->format('m-d-Y'); ?>" data-date-format="MM/DD/YYYY" id="input-option227" class="form-control">
				<input type="hidden" name="oldDate" id="oldDate" value="<?php echo (new DateTime($autoship_info['deliverydate']))->format('m-d-Y'); ?>">
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
            </div>
            <div>                   
LAST AUTO-DELIVERY ORDER DATE: <br/>
<?php if (isset($autoship_info['lastdeliverydate'])) { ?>
<strong><?php echo (new DateTime($autoship_info['lastdeliverydate']))->format('m-d-Y'); ?></strong>
<?php }  else { ?>
<strong>&nbsp;</strong> 
<?php } ?>
            </div>

            <div>                   
NEXT AUTO-DELIVERY ORDER DATE: <br/>
<strong id="next-delivery-date"><?php echo (new DateTime($autoship_info['nextdeliverydate']))->format('m-d-Y'); ?></strong>
            </div>
            <div>&nbsp;</div>
                    
                        <button type="button" data-loading-text="Loading..." class="btn btn-primary btn-lg btn-block" onclick="doUpdate();"  id="update_autoship">Update ADO</button>
                <div>&nbsp;</div>        
     <button type="button" data-loading-text="Loading..." class="btn btn-danger btn-lg btn-block" onclick="cancelAutoship();"  id="cancelAutoship">Cancel ADO</button>
              <div><hr/></div>   
     <button type="button" data-loading-text="Loading..." class="btn btn-primary btn-lg btn-block" onclick="payshipToday();"  id="payshipToday">Pay & Ship Today</button>
                <div>&nbsp;</div>        


<div class="list-group">
    <a href="<?php echo $autoship_link_history; ?>" class="list-group-item">ADO Transaction History</a>
    <a href="/bi_ledger.cgi" class="list-group-item">Billing Information</a> 
    
  </div>            
            
            
            
            </div>
                     
                     
                     
                     
                     
                      </div>
  <?php } else {  ?>                 
    <h3><a href="/bi.cgi">Click here</a> to update your Billing Information</h3>

<?php } ?>
      <?php echo $content_bottom; ?></div>


    <?php echo $column_right; ?></div>
</div>



<script type="text/javascript" src="admin/view/javascript/jquery/datetimepicker/moment.js"></script>
<script type="text/javascript" src="admin/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery.numeric.min.js"></script>

<script type="text/javascript"><!--


function cancelAutoship(){

swal({
  title: 'Cancel ADO?',
  text: "You won't be able to revert this!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',  
  confirmButtonText: 'Yes, cancel it!',
  cancelButtonText: 'No, close this dialog!',
},function () {
        jQuery.ajax({url:'index.php?route=extension/module/autoship/activateAutoship',data:{user_id:'<?php echo $customer_id; ?>',active_status:0},dataType:'POST',success:function(data){      
        

                
        },
        complete:function(){
          swal(
        {title:'ADO Cancelled!',
        text:'Your autoship subscription has been cancelled.',
        type:'success'
      },function(){
          location.reload();    
          
          });
          
            }});
    

}, function (dismiss) {
  // dismiss can be 'cancel', 'overlay',
  // 'close', and 'timer'
  if (dismiss === 'cancel') {
    swal(
      ':)',
      'Your autoship is still on',
      'error'
    )
  }
});




}


function getAmount(amount){
    return amount.replace(/[^\d.-]/g, '');

}
function checkDate(){
	var mydatetimepicker = $('#input-option227').datetimepicker();
    
    var date = moment($("#input-option227").val(),"MM/DD/YYYY");
	var selectedDate = date.toDate();

	var now = new Date();
	
	var isInvalid =false;
	var intputDate = $("#input-option227").val();
	
	var comp = null;
	if(intputDate.includes("-")){
	    comp = intputDate.split('-'); 
	}else{ 
	    comp = intputDate.split('/');
    }

	if(comp != null){
		var m = parseInt(comp[0], 10);
		var d = parseInt(comp[1], 10);
		var y = parseInt(comp[2], 10);
			
		var date2 = new Date(y,m-1,d);
		
		if (date2.getFullYear() == y && date2.getMonth() + 1 == m && date2.getDate() == d) {
		  isInvalid =false;
		} else {
		  isInvalid =true;
		}
	}else{
		isInvalid= true;
	}
 
	
	
	
	now.setHours(0,0,0,0);
	if (isInvalid) {
		  swal(
			  'Invalid Date',
			  'Please enter a valid date!',
			  'error'
			).then(function(){
			$("#input-option227").val("");
		});
		return true; 
	}else if (selectedDate <= now) {
		  swal(
			  'Invalid Date',
			  'Please enter a future date!',
			  'error'
			).then(function(){
			$("#input-option227").val("");
		});
		return true; 
	}
	return false;
}

function checkDate2(){
	var mydatetimepicker = $('#input-option227').datetimepicker();
    
    var date = moment($("#input-option227").val(),"MM/DD/YYYY");
	var selectedDate = date.toDate();

	var now = new Date();
	
	var isInvalid =false;
	var intputDate = $("#input-option227").val();
	
	var comp = null;
	if(intputDate.includes("-")){
	    comp = intputDate.split('-'); 
	}else{ 
	    comp = intputDate.split('/');
    }

	if(comp != null){
		var m = parseInt(comp[0], 10);
		var d = parseInt(comp[1], 10);
		var y = parseInt(comp[2], 10);
			
		var date2 = new Date(y,m-1,d);
		
		if (date2.getFullYear() == y && date2.getMonth() + 1 == m && date2.getDate() == d) {
		  isInvalid =false;
		} else {
		  isInvalid =true;
		}
	}else{
		isInvalid= true;
	}
 
	now.setHours(0,0,0,0);
	if (isInvalid) {
		  swal(
			  'Invalid Date',
			  'Please enter a valid date!',
			  'error'
			).then(function(){
			$("#input-option227").val("");
		});
		return true; 
	}
	return false;
}

function checkDate3(){
	var mydatetimepicker = $('#input-option227').datetimepicker();
    
    var date = moment($("#input-option227").val(),"MM/DD/YYYY");
	var selectedDate = date.toDate();

	var now = new Date();
	
	now.setHours(0,0,0,0);
	if (selectedDate <= now) {
		
		return true; 
	}
	return false;
}

function doUpdate(){
    var mydatetimepicker = $('#input-option227').datetimepicker();
    
    var date = moment($("#input-option227").val(),"MM/DD/YYYY");
	var _deliverydate = date.format("YYYY-MM-DD");
	if(checkDate2()){
		return;
	}
	if(checkDate3()){
		
		_deliverydate = "noUpdate";
	
	}
	
    console.log(_deliverydate);
	//var _nextdeliverydate = $('#next-delivery-date').text();
	//console.log($('#next-delivery-date').text());
    var _total = jQuery(".the_overall_total").text();
        _total = getAmount(_total);
    jQuery.ajax({url:'index.php?route=extension/module/autoship/updateautoship',data:{deliverydate:_deliverydate,total:_total},dataType:'json',success:function(data){
            var total_amount = [];
            var price_list = [];
            var quantity = [];
            var product_ids = [];
			var status =[];
            jQuery(".gp_row").each(function(){    
					console.log(($(this).find("td").eq(2).text().trim()));
					var stat=0;
					if(($(this).find("td").eq(2).text().trim()) == 'Active'){
						stat=1;
						 total_amount.push(getAmount($(this).find("td").eq(4).text()));
					}else{
						 total_amount.push(getAmount($(this).find("td").eq(5).text()));
					}
					console.log(stat);
					status.push(stat); 
                    price_list.push(getAmount($(this).find("td").eq(3).text()));                
                    quantity.push($(this).find("input").val());             
                    product_ids.push($(this).attr("id").replace(/gp\-row\-child\-/g,""));                   
                  //  total_amount.push(getAmount($(this).find("td").eq(4).text()));          
                }); 
                jQuery.ajax({url:'index.php?route=extension/module/autoship/updateautoshipdetails',data:{autoship_id:data.result_id,prices:price_list,quantities:quantity,product_idss:product_ids,total_amounts:total_amount,status:status},dataType:'json',success:function(data2){


                    
                    }});
                
            jQuery("#next-delivery-date").text(date.format("MM-DD-YYYY"));
            jQuery("#success_message").remove();
            jQuery("#content").before('<div class="alert alert-success"  id="success_message"><i class="fa fa-check-circle"></i> Success! Done saving ADO info.<button data-dismiss="alert" class="close" type="button">×</button></div>');





        },complete:function(){
            
            jQuery.ajax({url:'/cart/index.php?route=extension/module/autoship/refreshAutoshipSummary',data:{},type:'GET',dataType:'json',success:function(data2){
                $("#autoship_count").text(data2.count);
                $("#autoship_total").text(data2.total);
                                     
            }
			
			});
            
            }});
			

}

function doUpdateAndGotoPage(){
    var mydatetimepicker = $('#input-option227').datetimepicker();
    
    var date = moment($("#input-option227").val(),"MM/DD/YYYY");
	

	
    var _deliverydate = date.format("YYYY-MM-DD");
	if(checkDate3()){
		
		_deliverydate = "noUpdate";
	
	}
    var _total = jQuery(".the_overall_total").text();
        _total = getAmount(_total);
    jQuery.ajax({url:'index.php?route=extension/module/autoship/updateautoship',data:{deliverydate:_deliverydate,total:_total},dataType:'json',success:function(data){
            var total_amount = [];
            var price_list = [];
            var quantity = [];
            var product_ids = [];
			var status =[];
		
            jQuery(".gp_row").each(function(){    
					console.log(($(this).find("td").eq(2).text().trim()));
					var stat=0;
					if(($(this).find("td").eq(2).text().trim()) == 'Active'){
						stat=1;
						 total_amount.push(getAmount($(this).find("td").eq(4).text()));
					}else{
						 total_amount.push(getAmount($(this).find("td").eq(5).text()));
					}
					console.log(stat);
					status.push(stat);
                    price_list.push(getAmount($(this).find("td").eq(3).text()));                
                    quantity.push($(this).find("input").val());             
                    product_ids.push($(this).attr("id").replace(/gp\-row\-child\-/g,""));                   
                    //total_amount.push(getAmount($(this).find("td").eq(4).text()));          
                }); 
                jQuery.ajax({url:'index.php?route=extension/module/autoship/updateautoshipdetails',data:{autoship_id:data.result_id,prices:price_list,quantities:quantity,product_idss:product_ids,total_amounts:total_amount,status:status},dataType:'json',success:function(data2){


                    
                    }});
                
            jQuery("#next-delivery-date").text(date.format("MM-DD-YYYY"));
            jQuery("#success_message").remove();
            //jQuery("#content").before('<div class="alert alert-success"  id="success_message"><i class="fa fa-check-circle"></i> Success! Done saving ADO info.<button data-dismiss="alert" class="close" type="button">×</button></div>');

        },complete:function(){
            
            jQuery.ajax({url:'/cart/index.php?route=extension/module/autoship/refreshAutoshipSummary',data:{},type:'GET',dataType:'json',success:function(data2){
                $("#autoship_count").text(data2.count);
                $("#autoship_total").text(data2.total);
                                     
            },
            complete:function(){
                window.location.href = '/cart/autoships/';
                
                }
            });
            
            
            
            
            }});

}


function getCurrency(the_amount){

    var currencyString = the_amount.replace(/\d+/g, '');
        currencyString = currencyString.replace(/\./g, '').trim();
        currencyString = currencyString.replace(/\,/g, '').trim();
        return currencyString;

}

function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

function totalIt(){
    var the_total = 0;
    jQuery(".totalcell").each(function(){
            var the_price = jQuery(this).text();
            the_price = the_price.replace(/[^\d.-]/g, '');
            the_total = the_total + parseFloat(the_price);
        });
    
    var shipping_total = getAmount(jQuery(".ship_total").text());

    the_total = the_total == 0.0 ? 0.0 : the_total + parseFloat(shipping_total);
    
    var currencyString = getCurrency($(".the_overall_total").text());
        
    var the_commas = numberWithCommas(the_total.toFixed(2));
    
    jQuery(".the_overall_total").text(currencyString + the_commas);
}
    
function updateItem(itemid){

 
 var the_price = $('#gp-row-child-' + itemid + ' td').eq(3).text();

var currencyString = getCurrency(the_price);
     the_price = getAmount(the_price); 
    var the_computed = parseFloat(the_price) * parseFloat($('#quantity-' + itemid).val()) 
    var the_commas = numberWithCommas(the_computed.toFixed(2));
    $('#gp-row-child-' + itemid + ' td').eq(4).text(currencyString + the_commas);
    totalIt();
}

function removeItem(itemid){
    
swal({
  title: 'Are you sure you want to delete this item?',
  text: "You are deleting " + $('#gp-row-child-' + itemid ).children('td:first').text() + " in the list." ,
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',  
  confirmButtonText: 'Yes, Delete it!',
  cancelButtonText: 'No, close this dialog!',
},function () {
        $('#gp-row-child-' + itemid).remove();
        totalIt();

}, function (dismiss) {
  if (dismiss === 'cancel') {
    swal(
      ':)',
      'Nothing is deleted',
      'error'
    )
  }
});  
    
    
}   

function deactivateItem(itemid){
    
swal({
  title: 'Are you sure you want to Deactivate this item?',
  text:"You will no longer be charged monthly for \n"+$('#gp-row-child-' + itemid ).children('td:first').text()+".",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',  
  confirmButtonText: 'Yes, Deactivate it!',
  cancelButtonText: 'No, close this dialog!',
},function () {
      //  $('#gp-row-child-' + itemid).remove();
	
	  $('#gp-td-child-'+itemid+'-fprice').html("<label>$0.00</label>");
        totalIt();
		
	    jQuery.ajax({url:'index.php?route=extension/module/autoship/updateProdStatus',data:{user_id:'<?php echo $customer_id; ?>',active_status:0,product_id:itemid},dataType:'POST',success:function(data){      
        

                
        },
        complete:function(){
          swal(
        {title:'Product is Deactivated!',
        text:'',
        type:'success'
      },function(){
          location.reload();    
          
          });
          
            }});

}, function (dismiss) {
  if (dismiss === 'cancel') {
    swal(
      ':)',
      'Nothing has been deactivated',
      'error'
    )
  }
});  
    
    
}   


function reactiveItem(itemid){
    
swal({
  title: 'Are you sure you want to Reactivate this item?',
  text:"Thank you for re-activating "+$('#gp-row-child-' + itemid ).children('td:first').text()+". Your next charge would be on, "+ $('#next-delivery-date').text()+".",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',  
  confirmButtonText: 'Yes, Re-Activate it!',
  cancelButtonText: 'No, close this dialog!',
},function () {
      //  $('#gp-row-child-' + itemid).remove();
	
	  //$('#gp-td-child-'+itemid+'-fprice').html("<label>$0.00</label>");
        totalIt();
		
	    jQuery.ajax({url:'index.php?route=extension/module/autoship/updateProdStatus',data:{user_id:'<?php echo $customer_id; ?>',active_status:1,product_id:itemid},dataType:'POST',success:function(data){      
        

                
        },
        complete:function(){
          swal(
        {title:'Product is Re-Activated!',
        text:'',
        type:'success'
      },function(){
          location.reload();    
          
          });
          
            }});

}, function (dismiss) {
  if (dismiss === 'cancel') {
    swal(
      ':)',
      'Nothing has been Re-Activated',
      'error'
    )
  }
});  
    
  

}   
    

function payshipToday(){

	$(".input-quantity-value").each(function(){
		//console.log($(this).attr("id"));
		product_id = $(this).attr("id").replace("quantity-","");
		console.log("quantity is " + $(this).val());
		cart.add(product_id,$(this).val());	

	});



	
swal({
  title: 'ADO Items added for checkout',  
  type: 'info',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',  
  confirmButtonText: 'Checkout Now!',
  cancelButtonText: 'No, close this dialog!',
},function(isConfirm) {
	$(".alert").hide();
	 if (isConfirm) {	
		location = "/cart/index.php?route=checkout/checkout";	
		}	
});



}

    
    
function gpAddChild(theInput, theFilter) {
    theInput.autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=extension/module/autoship/autocomplete&filter_' + theFilter + '=' +  encodeURIComponent(request),
                dataType: 'json',           
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: (theFilter == 'name') ? item['name'] : item['model'],
                            value: item['product_id'],
                            name: item['name'],
                            model: item['model'],
                            price: item['price']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            theInput.val('');
            $('#gp-row-child-' + item['value']).remove();

            html  = '<tr id="gp-row-child-' + item['value'] + '" class="gp_row">';
            html += '  <td class="text-left">' + item['name'] + '</td>';
            html += '  <td class="text-left">';
            html += '  <div class="input-group btn-block" style="max-width: 200px;"> <input type="text"  id="quantity-' + item['value'] +'" name="quantity[' + item['value'] +']" value="1" size="1" class="form-control input-quantity-value">  <span class="input-group-btn"><button type="button"  onclick="updateItem(\'' + item['value'] +  '\');" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Update"><i class="fa fa-refresh"></i></button>            <button type="button" data-toggle="tooltip" title="" class="btn btn-danger" onclick="removeItem(\'' + item['value'] +  '\');" data-original-title="Remove"><i class="fa fa-times-circle"></i></button>       </span></div></td>';
            html += '  <td class="text-right">' + item['price'] + '</td>';
            html += '  <td class="text-right totalcell">';
            html +=  item['price']
            html += '  </td>';
            html += '</tr>';

            $('#gp-rows').append(html);
            totalIt();
        }
    });
}
//--></script>


<script type="text/javascript">
$(document).ready(function(){   
    
    document.title = "Xenesta Shopping Cart - Autoship Profile Page";

    $(".add-ado-product").on('click',function(e){
		
		if(!checkDate2()){
			$(this).attr("disabled",true);
			$(this).text("Please while system is saving the ADO profile..");        
			e.preventDefault();
			doUpdateAndGotoPage();
        }
        });

    
    $('.date').datetimepicker({pickTime: false,format: 'mm/dd/yyyy', minDate: 0 , constrainInput: true });
    

    
     $(".date").on("dp.change", function (e) {         


            var date = moment($("#input-option227").val(),"MM/DD/YYYY");

            var selectedDate = date.toDate();

            var now = new Date();
            now.setHours(0,0,0,0);
            if (selectedDate <= now) {
                  swal(
                      'Invalid Date',
                      'Please enter a future date!',
                      'error'
                    ).then(function(){
                    $("#input-option227").val("");
                });
                
            } 


        });
        
      $(".input-quantity-value").change(function(){
          console.log("current id is " + $(this).attr("id"));
          if ($(this).val() == 0) {
              
              removeItem($(this).attr("id").replace("quantity-",""));
          } 
      });
    
    
    totalIt();
    
    $(".gp_row input[type^=text]").numeric();
    
    $("#theform").submit(function(e){
        e.preventDefault(); 
    
        });
    

    
    
    

});

</script>



<?php echo $footer; ?>
