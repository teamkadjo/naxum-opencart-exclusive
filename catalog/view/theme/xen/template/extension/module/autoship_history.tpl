<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row">

<div id="content" class="col-sm-9">      <h1>Autoship Transaction History</h1>
        <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-right">Trans ID</td>
              <td class="text-left">Customer</td>
              <td class="text-right">Total</td>
              <td class="text-left">Product Count</td>
              <td class="text-left">Date Added</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
			<tr>
<?php foreach ($autoship_history as $ah) {  ?>

              <td class="text-right"><?php echo $ah['id']; ?></td>
              <td class="text-left"><?php echo $ah['fname']."  ".$ah['lname']; ?></td>
              <td class="text-right"><?php echo $ah['total']; ?></td>
              <td class="text-center"><?php echo $ah['itemscount']; ?></td>
              <td class="text-center"><?php echo $ah['dateupdated']; ?></td>
              <td class="text-right"><a href="<?php echo $ah['link']; ?>" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="View"><i class="fa fa-eye"></i></a></td>
            </tr>
<?php } ?>
                      </tbody>
        </table>
   
</div>
<!--
      <div class="row">
        <div class="col-sm-6 text-left"><ul class="pagination"><li class="active"><span>1</span></li><li><a href="//office.xenestalife.gmb/cart/index.php?route=account/order&amp;page=2">2</a></li><li><a href="//office.xenestalife.gmb/cart/index.php?route=account/order&amp;page=3">3</a></li><li><a href="//office.xenestalife.gmb/cart/index.php?route=account/order&amp;page=2">&gt;</a></li><li><a href="//office.xenestalife.gmb/cart/index.php?route=account/order&amp;page=3">&gt;|</a></li></ul></div>
        <div class="col-sm-6 text-right">Showing 1 to 10 of 22 (3 Pages)</div>
      </div>
-->
</div>


  <div class="col-sm-3">
     

<div class="list-group">
    <a href="<?php echo $autoship_link_history; ?>" class="list-group-item">Autoship Transaction History</a>
    <a href="<?php echo $bi_link; ?>" class="list-group-item">Billing Information</a> 
    
  </div>  
  
   
</div>         
        
      </div>

</div>


<?php echo $footer; ?>
