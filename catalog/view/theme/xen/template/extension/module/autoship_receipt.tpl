<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>

  <div class="row">

<div id="content" class="col-sm-9">      <h2>AUTOSHIP TRANSACTION</h2>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left" colspan="2">Transaction Details</td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left" style="width: 50%;">              <b>Transaction ID:</b> #<?php echo $id; ?><br>
              <b>Date Added:</b> <?php echo date('m/d/Y', strtotime($autoship_info['dateupdated'])); ?></td>
            <td class="text-left" style="width: 50%;">              <b>Payment Method:</b> <?php if ($trans_info['billmethod'] == "CC") { ?> Saved Credit Card # ****<?php echo $trans_info['last4number']; } ?><?php if ($trans_info['billmethod'] == "LEDGER") { ?> LEDGER <?php } ?><br>
                                          <b>Shipping Method:</b> Xen Shipping Rate              </td>
          </tr>
        </tbody>
      </table>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-left" style="width: 50%; vertical-align: top;">Payment Address</td>
                        <td class="text-left" style="width: 50%; vertical-align: top;">Shipping Address</td>
                      </tr>
        </thead>
        <tbody>
          <tr>
            <td class="text-left"><?php echo $trans_info['billfname']; ?> <?php echo $trans_info['billlname']; ?><br><?php echo $trans_info['billaddress']; ?><br>NULL<br><?php echo $trans_info['billcity']; ?>,<br><?php echo $trans_info['billcity']; ?><br><?php echo $trans_info['billcountry']; ?></td>
                        <td class="text-left"><?php echo $trans_info['billfname']; ?> <?php echo $trans_info['billlname']; ?><br><?php echo $trans_info['billaddress']; ?><br>NULL<br><?php echo $trans_info['billcity']; ?>,<br><?php echo $trans_info['billcity']; ?><br><?php echo $trans_info['billcountry']; ?></td>
                      </tr>
        </tbody>
      </table>
      <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-left">Product Name</td>
              <td class="text-left">Model</td>
              <td class="text-right">Quantity</td>
              <td class="text-right">Price</td>
              <td class="text-right">Total</td>                           
                          </tr>
          </thead>
          <tbody>
              
              <?php foreach ($autoship_details as $od ) { ?>
              
                        <tr>
              <td class="text-left"><?php echo html_entity_decode($od['name']); ?>                </td>
              <td class="text-left"><?php echo $od['model']; ?></td>
              <td class="text-right"><?php echo $od['quantity']; ?></td>
              <td class="text-right"><?php echo $od['price']; ?></td>
              <td class="text-right"><?php echo $od['total']; ?></td>
            </tr>
            
                <?php } ?>
                                  </tbody>
          <tfoot>
                        <tr>
              <td colspan="3"></td>
              <td class="text-right"><b>Sub-Total</b></td>
              <td class="text-right"><?php echo $autoship_info['total'] - $autoship_info['shipping_total']; ?></td>

                          </tr>
                        <tr>
              <td colspan="3"></td>
              <td class="text-right"><b>Xen Shipping Rate</b></td>
              <td class="text-right">   <?php echo $autoship_info['shipping_total']; ?></td>

                          </tr>
                        <tr>
              <td colspan="3"></td>
              <td class="text-right"><b>Total</b></td>
              <td class="text-right">$ <?php echo $autoship_info['total']; ?></td>
                          </tr>
                      </tfoot>
        </table>
      </div>
    
            <div class="buttons clearfix">
        <div class="pull-right"><a href="/cart/index.php?route=extension/module/autoship/viewhistory" class="btn btn-primary">Continue</a></div>
      </div>
      </div>    
    
    
	</div>
</div>
<?php echo $footer; ?>
