Dear  <?php echo $customer_name; ?>:<br/>
<p>
We encountered a problem processing your credit card for your
latest Auto Delivery Order (ADO).
</p>

<p>
<?php if ($info_try_cc) { ?>
        <p style="margin-top: 0px; margin-bottom: 3px;"><?php echo $info_try_cc; ?></p>
<?php } ?>
This transaction has been declined by your financial institution.
The error returned by your financial institution was:
</p>
<?php echo $error_message_card; ?><br/>
<p>
We were using the credit card account number : <?php echo $credit_card_digits4; ?><br/>
</p>

<p>
Your Auto Delivery Order (ADO)  has been deactivated. In order to have activate your ADO you will need to update your billing information, change the next auto ship date, and activate your ADO.
</p>
<p>
Please click the following link to check the billing information we currently have on file for you and - if necessary - to update that information. You will need the site name and password that you ordinarily use for your Xenesta Life web site to login here.
</p>
<p>
<a href="<?php echo $billing_info_site; ?>"><?php echo $billing_info_site; ?></a><br/>
Site Name: <?php echo $site_name; ?><br/>
</p>
<p>
You may contact Xenestalife Support if you have any questions about this by email support@xenestalife.com and we will respond promptly.
</p>

Respectfully,<br/>
Xenestalife Billing Department
