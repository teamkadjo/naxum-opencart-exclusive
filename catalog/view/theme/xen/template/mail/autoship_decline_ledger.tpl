Dear  <?php echo $customer_name; ?>:<br/>
<p>
We encountered a problem processing your
latest Auto Delivery Order (ADO).
</p>

<p>
This transaction has been declined due to insufficient funds on your Ledger account.
</p>
<br/>
<p>
Your Auto Delivery Order (ADO) is not processed this month. In order to activate your ADO you will need to update your billing information, change the next auto ship date, and activate your ADO. You may also choose the "Pay and Ship" option in order to purchase your products today.
</p>
<p>
Please click the following link to check the billing information we currently have on file for you and - if necessary - to update that information. You will need the site name and password that you ordinarily use for your Xenesta Life web site to login here.
</p>
<p>
<a href="<?php echo $billing_info_site; ?>"><?php echo $billing_info_site; ?></a><br/>
Site Name: <?php echo $site_name; ?><br/>
</p>
<p>
You may contact Xenestalife Support if you have any questions about this by email support@xenestalife.com and we will respond promptly.
</p>

Respectfully,<br/>
Xenestalife Billing Department
