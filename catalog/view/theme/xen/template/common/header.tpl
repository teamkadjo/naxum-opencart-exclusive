<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>

<link rel="stylesheet" href="/style/rpa/rpa-backoffice.css"/>
<link rel="stylesheet" href="/style/rpa/rpa-menu.css"/>
<link rel="stylesheet" href="/style/profile/responsive-jm.css"/>
<script src="js/rpa-backoffice.js" type="text/javascript"></script>



<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="/js/jquery.tools.min.js" type="text/javascript"></script>
<script src="/js/hideElem.js" type="text/javascript"></script>
<script src="/js/notif.js" type="text/javascript"></script>
<link href="/css/bootstrap-datetimepicker.css" rel="stylesheet" media="screen">
<link href="/css/star-rating.min.css" rel="stylesheet" media="screen">
<script src="/js/respond.min.js"></script>
<script src="/js/bootstrap-maxlength.min.js"></script>
<script type="text/javascript" src="/js/jquery.autocomplete.js"></script>
<script src="/js/sweetalert.js"></script>
<!--cdns 
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="js/jquery-1.11.1.min.js" type="text/javascript"></script>
-->

<!-- <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> -->
<link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<link href="/css/common/editor/froala_editor.min.css" rel="stylesheet" type="text/css" />
<link href="/css/common/editor/froala_style.min.css" rel="stylesheet" type="text/css" />
    
  
  
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!--end cdns -->


<!--fonts -->
<link href='//fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Oswald" />

<!--fonts -->
<!-- start customized views -->
<link href="/css/systems/xen/custom.css?session-30166725751860" rel="stylesheet">
<link href="/css/systems/xen/common.css?session-30166725751860" rel="stylesheet" media="screen">
 <!-- end customized views --> 

 
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


    <!-- MENUS -->
    
<link href="/css/systems/xen/menu.css?session-30166725751860" rel="stylesheet">
<link href="/css/systems/xen/menu-custom.css?session-30166725751860" rel="stylesheet">
<link href="/css/global.min.css?session-30166725751860" rel="stylesheet">

    <!-- MENUS -->
    
    
<script src="/js/moment.min.js"></script>


<script src="/js/livestamp.min.js"></script>
<script src="/js/bootstrap-notify.min.js"></script>


<script src="/js/star-rating.min.js"></script>
<link href="/css/sweetalert.css" rel="stylesheet">
<script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>



<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css"/>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>


<link rel="stylesheet" href="//cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css"/>
<script src="//cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>

<script src="/js/jquery.cookie.js"></script>
<script src="/js/jquery.md5.js"></script>




<link href="catalog/view/theme/xen/stylesheet/stylesheet.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery.ajax({'url':"../cart_header.cgi",success:function(data){
			var pattern = /href\=\"\//g;
			the_string = data.replace(pattern, "href=\"");
			pattern = /href\=\"/g;
			the_string = the_string.replace(pattern, "href=\"/");
            the_string = the_string.substring(the_string.indexOf('<nav class="navbar navbar-default top-nav">'));		
            //the_string = the_string.split('<nav class="navbar navbar-default top-nav">').pop();
			jQuery("#top").html(the_string);
	},
    complete:function(){
        
        
        }
    });

});


</script>
<script src="/js/sweetalert.js"></script>

<body class="<?php echo $class; ?>">
<div id="top">
	<div>&nbsp;&nbsp;&nbsp;Loading...</div>
</div>
<header>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div class="btn-group btn-block " >
<a href="<?php echo $autoship_link; ?>"  class="btn btn-primary  btn-block" data-toggle="tooltip" title="<?php echo $text_auto_tooltip ?>" >Auto Delivery Order Profile (ADO)<?php if ($autoship_info['is_active']) { ?> -  [ <span id="autoship_count"><?php echo $autoship_summary['count'] ?></span> item(s) - <span id="autoship_total"><?php echo $autoship_summary['total'] ?></span> ] <?php } ?> </a>
        </div>
      </div>
      <div class="col-sm-5"><?php echo $search; ?>
      </div>
      <div class="col-sm-3"><?php echo $cart; ?></div> 
    </div>
  </div>
</header>
<?php if ($categories) { ?>
<div class="container">
  <nav id="menu" class="navbar">
    <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
      <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
    </div>
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav">
        <?php foreach ($categories as $category) { ?>
			<?php if (($category['category_id'] == 1 ) && (!$autoship_active)) { continue;} ?> 
        <?php if ($category['children']) { ?>
        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
          <div class="dropdown-menu">
            <div class="dropdown-inner">
              <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
              <ul class="list-unstyled">
                <?php foreach ($children as $child) { ?>
                <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                <?php } ?>
              </ul>
              <?php } ?>
            </div>
            <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
        </li>
        <?php } else { ?>
        <li><a  href="<?php echo $category['href']; ?>"  data-toggle="tooltip" title="<?php echo $text_cat_tooltip?>

<?php  echo (strpos(strtolower($category['name']),"book") !==false ?  "Xenesta Books" : $category['name'])?> "><?php echo $category['name']; ?></a></li>
        <?php } ?>
        <?php } ?>
      </ul>
    </div>
  </nav>
</div>
<?php } ?>

